/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <stdio.h>

#include "lexer.h"
#include "gpfnode.h"
#include "common.h"

#define INITIAL_STATE 0
#define GPF_UNSTARTABLE_STATE -1

static GpfDocument *global_document;

static void apply_marking (GpfNode *node, GpfDocument *document);
static void lex_phase (GpfDocument *document);
static GpfToken *fix_lookbacks (GpfDocument *document, GpfToken *token);
static void mark_from (GpfToken *token, GpfDocument *document);
static GpfToken *first_new_token (GpfToken *token, GpfDocument *document);

void
gpf_lex (GpfDocument *document)
{
  global_document = document;
  apply_marking (document->ultra_root, document);
  lex_phase (document);
  gpf_update_lookbacks (document);
}

GpfToken *
gpf_relex (GpfToken *token, GpfDocument *document)
{
  global_document = document;
  return first_new_token (token, document);
}

void
gpf_mark_tokens (GpfDocument *document)
{
  apply_marking (document->ultra_root, document);
}


/* Locate all the edit sites within node.  Call mark_from() on each
   edited terminal and the boundaries of each structural edit. */
static void
apply_marking (GpfNode *node, GpfDocument *document)
{
  int i;

  if (!node)
    return;

  if (GPF_IS_TOKEN(node)
      && gpf_node_has_changes(node, GPF_TEXT_CHANGES, GPF_REFERENCE_VERSION))
    mark_from(GPF_TOKEN(node), document); /* Handle textual changes. */
  else {
    /* Handle structural changes. */
    if (gpf_node_has_changes(node, GPF_CHILD_CHANGES, GPF_REFERENCE_VERSION)) {
      GpfNode *old_child;

      for (i = 0; i < gpf_node_arity (node, GPF_CURRENT_VERSION); i++) {
        old_child = gpf_node_get_child (node, i, GPF_REFERENCE_VERSION);
        if (old_child != gpf_node_get_child (node, i, GPF_CURRENT_VERSION)) {
          /* Mark first token not earlier than the leading edge of the
             original subtree. */
          mark_from(_gpf_first_token (old_child, GPF_REFERENCE_VERSION), document);
          /* Mark first token after the original subtree. */
          mark_from(_gpf_first_token_after (old_child, GPF_REFERENCE_VERSION), document);
        }
      }
    }

    /* Recursively process any edits within this subtree. */
    if (gpf_node_has_changes(node, GPF_NESTED_CHANGES, GPF_REFERENCE_VERSION))
      for (i = 0; i < gpf_node_arity (node, GPF_CURRENT_VERSION); i++)
        apply_marking(gpf_node_get_child (node, i, GPF_CURRENT_VERSION), document);
  }
}


/* Ensure that we have a valid state to re-start the lexer here. This
   backup occurs in the current tree. */
void
ensure_startable (GpfToken *token)
{
  GpfToken *token2;
  for (token2 = gpf_token_previous (token, GPF_CURRENT_VERSION);
       token2 && (gpf_token_get_state (token2, GPF_CURRENT_VERSION)
                  == GPF_UNSTARTABLE_STATE)
       && !gpf_token_is_marked (token2);
       token2 = gpf_token_previous (token2, GPF_CURRENT_VERSION))
    gpf_token_mark (token2);
}

/* Explicitly mark tokens dependent upon tok for re-lexing. This
   backup occurs in the reference version. */
void
mark_from (GpfToken *token, GpfDocument *document)
{
  int ov;

  if (!gpf_node_exists(GPF_NODE(token), GPF_CURRENT_VERSION)
      || !gpf_node_exists(GPF_NODE(token), GPF_REFERENCE_VERSION))
    return;

  ensure_startable(token);
  gpf_token_mark (token);
  
  /* Check everything in its lookback set. */
  for (ov = gpf_token_get_lookback (token, GPF_CURRENT_VERSION); ov > 0; --ov) {
    token = gpf_token_previous (token, GPF_REFERENCE_VERSION);
    if (token == document->bos)
      return;
    if (!gpf_node_exists (GPF_NODE(token), GPF_CURRENT_VERSION)
        || gpf_token_is_marked (token))
      continue;
    gpf_token_mark (token);
    ensure_startable (token);
  }
}

static GpfNode *
next_subtree (GpfNode *node, GpfVersion version)
{
  GpfNode *subtree;

  if (node == NULL)
    return NULL;

  subtree = gpf_node_right_sibling (node, version);

  if (subtree)
    return subtree;

  return next_subtree (gpf_node_get_parent (node, version), version);
}

extern void batch_lexer_set_state (int state);
extern int batch_lexer_get_state ();
extern GSList *batch_lexer_more_tokens(GpfDocument *document);


static GSList *token_list;
static GpfToken *read_token;
static GpfToken *construction_token;
static GpfToken *last_token;
static GpfToken *last_reused_token;
static int read_offset;
static int lexeme_offset;
static int construction_offset;
static char *read_token_lexeme;

/* Figure 5.17: Computing bottom-up reuse during incremental
   lexing. next_new_token is modified to apply this test to all tokens
   passed over when updating the construction location through calls
   to advance. If an old token can be reused, the new information is
   copied into its fields. (Although more aggressive strategies could
   be employed, they are typically subsumed by top-down reuse. The
   simple scheme shown here captures the common cases and creates
   additional top-down reuse possibilities by 'seeding' the discovery
   process.)  */

#if 0
gboolean
bottom_up_reuse_test (GpfToken *token)
{
  if (gpf_node_get_symbol (construction_token, GPF_CURRENT_VERSION)
      == gpf_node_get_symbol (token, GPF_CURRENT_VERSION) &&
      construction_token != last_reused_token &&
      construction_token != document->eos) {
    gpf_token_set_state (construction_token, gpf_token_get_state (token,
    construction_token->lexeme = tok->lexeme;
    construction_token->lookahead = tok->lookahead;
    /* Treat this token as re-lexed during lookback update phase. */
    set_tok_was_re_lexed(construction_token, true);
    last_reused_token = construction_token;
    return TRUE;
  }
  return FALSE;
}

#endif

/* Return the next re-lexed token. */
GpfToken *
gpf_next_new_token ()
{
  GSList *cur;

  if (token_list == NULL)
    token_list = batch_lexer_more_tokens(global_document);
  for (cur = token_list; cur != NULL; cur = cur->next) {
    GpfToken *token = cur->data;

    if (cur->next == NULL)
      gpf_token_set_state (token, batch_lexer_get_state());
    else
      gpf_token_set_state (token, GPF_UNSTARTABLE_STATE);
    construction_offset += gpf_node_get_length (GPF_NODE (token),
                                                GPF_CURRENT_VERSION);
    gpf_token_set_lookahead (token, read_offset - construction_offset);
  }
  if (token_list == NULL)
    return global_document->eos;
  last_token = token_list->data;
  token_list = g_slist_remove (token_list, last_token);
  gpf_token_set_re_lexed (last_token);

  return last_token;
}

/* Begin incrementally lexing a new region starting at tok. */
static GpfToken *
first_new_token (GpfToken *token, GpfDocument *document)
{
  read_token = construction_token = token;
  construction_offset = read_offset = 0;
  read_token_lexeme = gpf_token_lexeme (read_token, GPF_CURRENT_VERSION);

  if (token == document->bos)
    batch_lexer_set_state(INITIAL_STATE);
  else {
    GpfToken *prev = gpf_token_previous(token, GPF_CURRENT_VERSION);
    batch_lexer_set_state(gpf_token_get_state (prev, GPF_CURRENT_VERSION));
  }
  token_list = NULL;

  return gpf_next_new_token(document);
}


/* Determine when previous and current token streams merge again. */
gboolean
can_stop_lexing (GpfDocument *document)
{
  return (token_list == NULL && construction_offset == 0
    && !gpf_token_is_marked(construction_token)
    && gpf_token_get_state (last_token, GPF_CURRENT_VERSION)
       == GPF_UNSTARTABLE_STATE
    && gpf_token_get_state (last_token, GPF_CURRENT_VERSION)
       == gpf_token_get_state (gpf_token_previous (construction_token,
                                                   GPF_PREVIOUS_VERSION),
                               GPF_CURRENT_VERSION));
}



/* Incremental run-time service provides this to batch lexer to read
   from lexemes in the previous version of the token stream.  */
int
gpf_next_char ()
{
  while (lexeme_offset == gpf_node_get_length(GPF_NODE (read_token),
                                              GPF_PREVIOUS_VERSION)
         && read_token != global_document->eos) {
    read_token = gpf_token_next (read_token, GPF_PREVIOUS_VERSION);
    read_token_lexeme = gpf_token_lexeme (read_token, GPF_PREVIOUS_VERSION);
    lexeme_offset = 0;
  }

  if (read_token == global_document->eos)
    return EOF;

  read_offset++;
  return read_token_lexeme[lexeme_offset++];
}

/* Find the next marked token within or after node. */
GpfToken *
find_next_region (GpfNode *node, GpfDocument *document)
{
  if (node == GPF_NODE (document->eos)
      || (GPF_IS_TOKEN(node) && gpf_token_is_marked(GPF_TOKEN (node))))
    return GPF_TOKEN (node);
  if (gpf_node_has_changes(node, GPF_NESTED_CHANGES, GPF_CURRENT_VERSION))
    return find_next_region(gpf_node_get_child(node, 0, GPF_CURRENT_VERSION), document);
  return find_next_region(next_subtree(node, GPF_CURRENT_VERSION), document);
}


/* Figure 5.12: Driver routine for the lexing phase, when used in a
   standalone fashion. Incremental lexing can be intermixed with
   parsing by having the incremental parser call the routines in
   Figure 5.11 directly.  Find and update each modified region of
   tokens.  */
static void
lex_phase (GpfDocument *document)
{
  GpfToken *token;

  global_document = document;
  for (token = find_next_region(document->ultra_root, document);
       token != document->eos;
       token = find_next_region(GPF_NODE (token), document)) {
    token = first_new_token(token, document);
    while (!can_stop_lexing(document))
      token = gpf_next_new_token();
  }
}

/* Figure 5.14: Driver routine for lookback recomputation.  Process a
   re-lexed region starting at tok.  */
void
gpf_update_lookbacks (GpfDocument *document)
{
  GpfNode *node = document->ultra_root;

  while (node) {
    if (GPF_IS_TOKEN(node)) {
      GpfToken *token = GPF_TOKEN (node);
      if (gpf_token_was_re_lexed(token))
        node = GPF_NODE (fix_lookbacks(document, token));
      else
        node = next_subtree(node, GPF_CURRENT_VERSION);
    } else if (gpf_node_has_changes(node, GPF_NESTED_CHANGES,
                                    GPF_CURRENT_VERSION))
      node = gpf_node_get_child(node, 0, GPF_CURRENT_VERSION);
    else
      node = next_subtree(node, GPF_CURRENT_VERSION);
  }
}

typedef struct _LookaheadElement {
  GpfToken *token;
  int cla;
  int cnt;
  struct _LookaheadElement *next;
} LookaheadElement;

typedef struct {
  LookaheadElement *elts;
  LookaheadElement *tail;
}LookaheadSet;

static void
lookahead_set_advance (LookaheadSet *la_set, int length)
{
  LookaheadElement *cur;

  for (cur = la_set->elts; cur != NULL; cur = cur->next) {
    cur->cla -= length;
    cur->cnt++;
  }
}

static int
lookahead_set_compute_lookback (LookaheadSet *la_set)
{
  LookaheadElement *cur = la_set->elts, *prev = NULL;
  int max_cnt = 0;

  while (cur != NULL) {
    if (cur->cnt > max_cnt)
      max_cnt = cur->cnt;

    if (cur->cla <= 0) {
      LookaheadElement *temp = cur;

      cur = cur->next;

      if (prev == NULL)
        la_set->elts = cur;
      else
        prev->next = cur;

      g_free (temp);
    } else {
      prev = cur;
      cur = cur->next;
    }
  }

  la_set->tail = prev;

  if (la_set->elts == NULL)
    return 0;
  else
    return max_cnt;
}

static void
lookahead_set_add_item (LookaheadSet *la_set, GpfToken *token)
{
  LookaheadElement *elt = g_new (LookaheadElement, 1);

  elt->token = token;
  elt->cla = gpf_token_get_lookahead (token, GPF_CURRENT_VERSION);
  elt->cnt = 0;
  elt->next = NULL;

  if (la_set->elts == NULL) {
    la_set->elts = elt;
    la_set->tail = elt;
  } else
    la_set->tail = la_set->tail->next = elt;
}

static gboolean
lookahead_set_all_items_discardable (LookaheadSet *la_set)
{
  LookaheadElement *cur;

  for (cur = la_set->elts; cur != NULL; cur = cur->next) {
    if (gpf_token_was_re_lexed (cur->token))
      return FALSE;
  }

  return TRUE;
}

/* Figure 5.15: Update algorithm for a contiguous range of modified
   tokens. The driver routine is shown in Figure 5.14. The operations on
   the list of lookaheads are defined in Figure 5.16.  */
static GpfToken *
fix_lookbacks (GpfDocument *document, GpfToken *token)
{
  LookaheadSet la_set;

  la_set.elts = NULL;
  la_set.tail = NULL;

  if (token != document->bos) {
    /* Extract lookback count (if different in current version, use
       old value). */
    int lb = gpf_token_get_lookback (
               gpf_token_next(gpf_token_previous (token,
                                                  GPF_CURRENT_VERSION),
                              GPF_REFERENCE_VERSION),
               GPF_REFERENCE_VERSION);
    GpfToken *boot_token = token;
    while (--lb > 0
           && gpf_token_previous (boot_token, GPF_REFERENCE_VERSION)
              == gpf_token_previous (boot_token, GPF_PREVIOUS_VERSION)
           && !gpf_token_was_re_lexed (
                 gpf_token_previous (boot_token, GPF_PREVIOUS_VERSION)))
      boot_token = gpf_token_previous(boot_token, GPF_CURRENT_VERSION);
    /* Initialize the lookahead set from the bootstrap region. */
    while (boot_token != token) {
      lookahead_set_advance (&la_set, gpf_node_get_length (GPF_NODE (boot_token), GPF_CURRENT_VERSION));
      lookahead_set_add_item (&la_set, boot_token);
      boot_token = gpf_token_next (boot_token, GPF_CURRENT_VERSION);
    }
  }
  do {
    /* Set the lookback for re-lexed tokens. */
    while (gpf_token_was_re_lexed(token)) {
      gpf_token_set_lookback (token, lookahead_set_compute_lookback(&la_set));
      lookahead_set_advance(&la_set, gpf_node_get_length(GPF_NODE(token), GPF_CURRENT_VERSION));
      lookahead_set_add_item(&la_set, token);
      token = gpf_token_next (token, GPF_CURRENT_VERSION);
    }
    /* Symmetric to bootstrap: process unmodified tokens reached by
       lookahead from re-lexed area. */
    while (token != document->eos && !gpf_token_was_re_lexed(token)
           && !lookahead_set_all_items_discardable(&la_set)
           && gpf_token_get_lookback (token, GPF_CURRENT_VERSION)
              != lookahead_set_compute_lookback(&la_set)) {
      gpf_token_set_lookback (token, lookahead_set_compute_lookback(&la_set));
      lookahead_set_advance(&la_set, gpf_node_get_length (GPF_NODE (token), GPF_CURRENT_VERSION));
      lookahead_set_add_item(&la_set, token);
      token = gpf_token_next (token, GPF_CURRENT_VERSION);
    }
  } while (gpf_token_was_re_lexed(token));
  return token; /* Return first clean token or eos to caller. */
}
