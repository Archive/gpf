/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <string.h>
#include "gpfdefaulttoken.h"

/* node states */
#define GPF_NODE_NEW     0
#define GPF_NODE_NORMAL  1
#define GPF_NODE_DELETED 1

/* class methods */
static void      gpf_default_token_base_class_init     (GpfDefaultTokenClass *class);
static void      gpf_default_token_base_class_finalize (GpfDefaultTokenClass *class);
static void      gpf_default_token_class_init          (GpfDefaultTokenClass *class);
static void      gpf_default_token_init                (GpfDefaultToken      *token);
/* node methods */
static int       gpf_default_token_get_symbol          (GpfNode     *token,
                                                        GpfVersion   version);
static void      gpf_default_token_set_symbol          (GpfNode     *token,
                                                        int          symbol);
static int       gpf_default_token_arity               (GpfNode     *token,
                                                        GpfVersion   version);
static int       gpf_default_token_get_length          (GpfNode     *token,
                                                        GpfVersion   version);
static void      gpf_default_token_set_length          (GpfNode     *token,
                                                        int          length);
static GpfNode  *gpf_default_token_get_parent          (GpfNode     *token,
                                                        GpfVersion   version);
static void      gpf_default_token_set_parent          (GpfNode     *token,
                                                        GpfNode     *parent);
static GpfNode  *gpf_default_token_get_child           (GpfNode     *token,
                                                        int          i,
                                                        GpfVersion   version);
static void      gpf_default_token_set_child           (GpfNode     *token,
                                                        int          i,
                                                        GpfNode     *child);
static gboolean  gpf_default_token_has_children        (GpfNode     *token,
                                                        GpfVersion   version);
static int       gpf_default_token_has_child           (GpfNode     *token,
                                                        GpfNode     *child,
                                                        GpfVersion   version);
static gboolean  gpf_default_token_has_changes         (GpfNode     *token,
                                                        int          type,
                                                        GpfVersion   version);
static gboolean  gpf_default_token_is_new              (GpfNode     *token);
static gboolean  gpf_default_token_exists              (GpfNode     *node,
                                                        GpfVersion version);
static void      gpf_default_token_discard_changes     (GpfNode     *token,
                                                        gboolean     and_nested);
static void      gpf_default_token_set_nested_changes  (GpfNode     *node);
static void      gpf_default_token_set_version         (GpfNode     *node,
                                                        GpfVersion   version);
static void      gpf_default_token_mark_deleted        (GpfNode     *token,
                                                        gboolean     and_nested);
static void      gpf_default_token_undelete            (GpfNode     *token,
                                                        gboolean     and_nested);
static void      gpf_default_token_commit              (GpfNode     *token,
                                                        gboolean     and_nested);
/* token methods */
static char     *gpf_default_token_lexeme              (GpfToken    *token,
                                                        GpfVersion   version);
static int       gpf_default_token_get_state           (GpfToken    *token,
                                                        GpfVersion   version);
static void      gpf_default_token_set_state           (GpfToken    *token,
                                                        int          state);
static int       gpf_default_token_get_lookahead       (GpfToken    *token,
                                                        GpfVersion   version);
static void      gpf_default_token_set_lookahead       (GpfToken    *token,
                                                        int          lookahead);
static int       gpf_default_token_get_lookback        (GpfToken    *token,
                                                        GpfVersion   version);
static void      gpf_default_token_set_lookback        (GpfToken    *token,
                                                        int          lookback);
static void      gpf_default_token_mark                (GpfToken    *token);
static gboolean  gpf_default_token_is_marked           (GpfToken    *token);
static void      gpf_default_token_set_re_lexed        (GpfToken    *token);
static gboolean  gpf_default_token_was_re_lexed        (GpfToken    *token);
static void      gpf_default_token_insert_text         (GpfToken    *token,
                                                        int          offset,
                                                        const char  *text,
                                                        int          length);
static void      gpf_default_token_delete_text         (GpfToken    *token,
                                                        int          start,
                                                        int          end);

#if 0
GType
gpf_default_token_get_type ()
{
  static GType type;

  if (!type) {
    static GTypeInfo info = {
      sizeof (GpfDefaultTokenClass),
      (GBaseInitFunc) gpf_default_token_base_class_init,
      (GBaseFinalizeFunc) gpf_default_token_base_class_finalize,
      (GClassInitFunc) gpf_default_token_class_init,
      NULL	/* class_destroy */,
      NULL	/* class_data */,
      sizeof (GpfDefaultToken),
      0		/* n_preallocs */,
      (GInstanceInitFunc) gpf_default_token_init ,
    };

    type = g_type_register_static (gpf_token_get_type (), "GpfDefaultToken", &info);
  }

  return type;
}
#endif

GtkType
gpf_default_token_get_type ()
{
  static int type;

  if (!type) {
    GtkTypeInfo info = {
	   "GpfDefaultToken",
	   sizeof (GpfDefaultToken),
	   sizeof (GpfDefaultTokenClass),
	   (GtkClassInitFunc) gpf_default_token_class_init,
	   (GtkObjectInitFunc) gpf_default_token_init,
	   (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };

    type = gtk_type_unique (gpf_token_get_type (), &info);
  }

  return type;
}

static void
gpf_default_token_base_class_init (GpfDefaultTokenClass *class)
{
  GpfNodeClass *node_class = GPF_NODE_CLASS (class);
  GpfTokenClass *token_class = GPF_TOKEN_CLASS (class);

  node_class->get_symbol = gpf_default_token_get_symbol;
  node_class->set_symbol = gpf_default_token_set_symbol;
  node_class->arity = gpf_default_token_arity;
  node_class->get_length = gpf_default_token_get_length;
  node_class->set_length = gpf_default_token_set_length;
  node_class->get_parent = gpf_default_token_get_parent;
  node_class->set_parent = gpf_default_token_set_parent;
  node_class->get_child = gpf_default_token_get_child;
  node_class->set_child = gpf_default_token_set_child;
  node_class->has_children = gpf_default_token_has_children;
  node_class->has_child = gpf_default_token_has_child;
  node_class->has_changes = gpf_default_token_has_changes;
  node_class->is_new = gpf_default_token_is_new;
  node_class->exists = gpf_default_token_exists;
  node_class->discard_changes = gpf_default_token_discard_changes;
  node_class->set_nested_changes = gpf_default_token_set_nested_changes;
  node_class->set_version = gpf_default_token_set_version;
  node_class->mark_deleted = gpf_default_token_mark_deleted;
  node_class->undelete = gpf_default_token_undelete;
  node_class->commit = gpf_default_token_commit;

  token_class->lexeme = gpf_default_token_lexeme;
  token_class->get_state = gpf_default_token_get_state;
  token_class->set_state = gpf_default_token_set_state;
  token_class->get_lookback = gpf_default_token_get_lookback;
  token_class->set_lookback = gpf_default_token_set_lookback;
  token_class->get_lookahead = gpf_default_token_get_lookahead;
  token_class->set_lookahead = gpf_default_token_set_lookahead;
  token_class->mark = gpf_default_token_mark;
  token_class->is_marked = gpf_default_token_is_marked;
  token_class->set_re_lexed = gpf_default_token_set_re_lexed;
  token_class->was_re_lexed = gpf_default_token_was_re_lexed;
  token_class->insert_text = gpf_default_token_insert_text;
  token_class->delete_text = gpf_default_token_delete_text;
}

static void
gpf_default_token_base_class_finalize (GpfDefaultTokenClass *class)
{
}

static void
gpf_default_token_class_init (GpfDefaultTokenClass *class)
{
  GpfNodeClass *node_class = GPF_NODE_CLASS (class);
  GpfTokenClass *token_class = GPF_TOKEN_CLASS (class);

  node_class->get_symbol = gpf_default_token_get_symbol;
  node_class->set_symbol = gpf_default_token_set_symbol;
  node_class->arity = gpf_default_token_arity;
  node_class->get_length = gpf_default_token_get_length;
  node_class->set_length = gpf_default_token_set_length;
  node_class->get_parent = gpf_default_token_get_parent;
  node_class->set_parent = gpf_default_token_set_parent;
  node_class->get_child = gpf_default_token_get_child;
  node_class->set_child = gpf_default_token_set_child;
  node_class->has_children = gpf_default_token_has_children;
  node_class->has_child = gpf_default_token_has_child;
  node_class->has_changes = gpf_default_token_has_changes;
  node_class->is_new = gpf_default_token_is_new;
  node_class->exists = gpf_default_token_exists;
  node_class->discard_changes = gpf_default_token_discard_changes;
  node_class->set_nested_changes = gpf_default_token_set_nested_changes;
  node_class->set_version = gpf_default_token_set_version;
  node_class->mark_deleted = gpf_default_token_mark_deleted;
  node_class->undelete = gpf_default_token_undelete;
  node_class->commit = gpf_default_token_commit;

  token_class->lexeme = gpf_default_token_lexeme;
  token_class->get_state = gpf_default_token_get_state;
  token_class->set_state = gpf_default_token_set_state;
  token_class->get_lookback = gpf_default_token_get_lookback;
  token_class->set_lookback = gpf_default_token_set_lookback;
  token_class->get_lookahead = gpf_default_token_get_lookahead;
  token_class->set_lookahead = gpf_default_token_set_lookahead;
  token_class->mark = gpf_default_token_mark;
  token_class->is_marked = gpf_default_token_is_marked;
  token_class->set_re_lexed = gpf_default_token_set_re_lexed;
  token_class->was_re_lexed = gpf_default_token_was_re_lexed;
  token_class->insert_text = gpf_default_token_insert_text;
  token_class->delete_text = gpf_default_token_delete_text;
}

static void
gpf_default_token_init (GpfDefaultToken *token)
{
  token->symbol = gpf_vobject_new ();
  token->lexeme = gpf_vobject_new ();
  token->parent = gpf_vobject_new ();
  token->state = gpf_vobject_new ();
  token->lookback = gpf_vobject_new ();
  token->lookahead = gpf_vobject_new ();
  token->lexer_state = gpf_vobject_new ();
  token->changed = FALSE;
  token->text_changes = FALSE;
  token->marked = FALSE;
}

#if 0
GpfToken *
gpf_default_token_new (int symbol, const char *lexeme, int length)
{
  GpfDefaultToken *token = GPF_DEFAULT_TOKEN (g_type_create_instance (gpf_default_token_get_type ()));
  GpfSTokenData *data = token->data;
  int i;

  data->symbol = symbol;
  data->lexeme = strdup (lexeme);
  data->length = length;
  data->parent = NULL;

  return GPF_TOKEN (token);
}
#endif

GpfToken *
gpf_default_token_new (int symbol, const char *lexeme, GpfNode *parent, int lookback,
                       int lookahead, int lexer_state, GpfDocument *document)
{
  GpfDefaultToken *token = GPF_DEFAULT_TOKEN (gtk_type_new (gpf_default_token_get_type ()));
  GpfNode *node = GPF_NODE (token);
  int state = GPF_NODE_NEW;
  char *real_lexeme;

  node->document = document;

  gpf_vobject_set (token->symbol, document->gvt, &symbol, sizeof (int));
  if (!lexeme) {
    real_lexeme = strdup ("");
  } else {
    real_lexeme = strdup (lexeme);
  }
  gpf_vobject_set (token->lexeme, document->gvt, &real_lexeme, sizeof (char *));
  if (parent)
    gpf_vobject_set (token->parent, document->gvt, &parent, sizeof (GpfNode *));
  gpf_vobject_set (token->state, document->gvt, &state, sizeof (int));
  gpf_vobject_set (token->lookback, document->gvt, &lookback, sizeof (int));
  gpf_vobject_set (token->lookahead, document->gvt, &lookahead, sizeof (int));
  gpf_vobject_set (token->lexer_state, document->gvt, &lexer_state, sizeof (int));

  return GPF_TOKEN (token);
}

static int
gpf_default_token_get_symbol (GpfNode *node, GpfVersion version)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);

  return gpf_vobject_get (default_token->symbol, int, node->document->gvt, version);
}

static void
gpf_default_token_set_symbol (GpfNode *node, int symbol)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);
  g_assert (node->document->editing);

  if (!default_token->changed)
    gpf_node_set_nested_changes (gpf_node_get_parent (node, GPF_CURRENT_VERSION));

  default_token->changed = TRUE;

  gpf_vobject_set (default_token->symbol, node->document->gvt, &symbol, sizeof (int));
}

static int
gpf_default_token_arity (GpfNode *node, GpfVersion version)
{
  return 0;
}

static int
gpf_default_token_get_length (GpfNode *node, GpfVersion version)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);

  return strlen (gpf_vobject_get (default_token->lexeme, char *, node->document->gvt, version));
}

static void
gpf_default_token_set_length (GpfNode *node, int length)
{
}

static GpfNode *
gpf_default_token_get_parent (GpfNode *node, GpfVersion version)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);

  return gpf_vobject_get (default_token->parent, GpfNode *, node->document->gvt, version);
}

static void
gpf_default_token_set_parent (GpfNode *node, GpfNode *parent)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);
  g_assert (node->document->editing);

  if (!default_token->changed)
    gpf_node_set_nested_changes (parent);

  default_token->changed = TRUE;

  gpf_vobject_set (default_token->parent, node->document->gvt, &parent, sizeof (GpfNode *));
}

static GpfNode *
gpf_default_token_get_child (GpfNode *node, int i, GpfVersion version)
{
  return NULL;
}

static void
gpf_default_token_set_child (GpfNode *node, int i, GpfNode *child)
{
}

static gboolean
gpf_default_token_has_children (GpfNode *node, GpfVersion version)
{
  return FALSE;
}

static int
gpf_default_token_has_child (GpfNode *node, GpfNode *child, GpfVersion version)
{
  return -1;
}

static gboolean
gpf_default_token_has_changes (GpfNode *node, int type, GpfVersion version)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);

  if (version == GPF_CURRENT_VERSION) {
    if (type & GPF_LOCAL_CHANGES)
      if (default_token->changed)
        return TRUE;
    if (type & GPF_TEXT_CHANGES)
     if (default_token->text_changes)
       return TRUE;
  } else {
    if (type & GPF_LOCAL_CHANGES)
      if (gpf_vobject_changed_over (default_token->symbol, node->document->gvt, node->document->gvt->global_version, version)
          || gpf_vobject_changed_over (default_token->lexeme, node->document->gvt, node->document->gvt->global_version, version)
          || gpf_vobject_changed_over (default_token->parent, node->document->gvt, node->document->gvt->global_version, version)
          || gpf_vobject_changed_over (default_token->state, node->document->gvt, node->document->gvt->global_version, version)
          || gpf_vobject_changed_over (default_token->lookback, node->document->gvt, node->document->gvt->global_version, version)
          || gpf_vobject_changed_over (default_token->lookahead, node->document->gvt, node->document->gvt->global_version, version)
          || gpf_vobject_changed_over (default_token->lexer_state, node->document->gvt, node->document->gvt->global_version, version))
        return TRUE;

    if (type & GPF_TEXT_CHANGES)
      if (gpf_vobject_changed_over (default_token->lexeme, node->document->gvt, node->document->gvt->global_version, version))
        return TRUE;
  }

  return FALSE;
}

static gboolean
gpf_default_token_is_new (GpfNode *node)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);

  if (default_token->state->vlog[0] == node->document->gvt->global_version)
    return TRUE;

  return FALSE;
}

gboolean
gpf_default_token_exists (GpfNode *node, GpfVersion version)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);

  return (gpf_vobject_get (default_token->state, int, node->document->gvt, version) != GPF_NODE_DELETED);
}


static void
gpf_default_token_discard_changes (GpfNode *node, gboolean and_nested)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);

  if (default_token->changed) {
    gpf_vobject_discard_changes (default_token->symbol);
    gpf_vobject_discard_changes (default_token->lexeme);
    gpf_vobject_discard_changes (default_token->parent);
    gpf_vobject_discard_changes (default_token->state);
    gpf_vobject_discard_changes (default_token->lookback);
    gpf_vobject_discard_changes (default_token->lookahead);
    gpf_vobject_discard_changes (default_token->lexer_state);
  }
}

static void
gpf_default_token_set_nested_changes (GpfNode *node)
{
}

static void
gpf_default_token_set_version (GpfNode *node, GpfVersion version)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);

  gpf_vobject_set_version (default_token->symbol, node->document->gvt, version);
  gpf_vobject_set_version (default_token->lexeme, node->document->gvt, version);
  gpf_vobject_set_version (default_token->state, node->document->gvt, version);
  gpf_vobject_set_version (default_token->parent, node->document->gvt, version);
  gpf_vobject_set_version (default_token->lookback, node->document->gvt, version);
  gpf_vobject_set_version (default_token->lookahead, node->document->gvt, version);
  gpf_vobject_set_version (default_token->lexer_state, node->document->gvt, version);
}

static void
gpf_default_token_mark_deleted (GpfNode *node, gboolean and_nested)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);
  int state = GPF_NODE_DELETED;

  gpf_vobject_set (default_token->state, node->document->gvt,
                   &state, sizeof (int));
}

static void
gpf_default_token_undelete (GpfNode *node, gboolean and_nested)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);
  if (gpf_vobject_changed (default_token->state))
    gpf_vobject_discard_changes (default_token->state);
}

static void
gpf_default_token_commit (GpfNode *node, gboolean and_nested)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (node);

  if (default_token->changed) {
    gpf_vobject_commit (default_token->symbol);
    gpf_vobject_commit (default_token->lexeme);
    gpf_vobject_commit (default_token->parent);
    gpf_vobject_commit (default_token->state);
    gpf_vobject_commit (default_token->lookback);
    gpf_vobject_commit (default_token->lookahead);
    gpf_vobject_commit (default_token->lexer_state);
  }

  default_token->changed = FALSE;
  default_token->text_changes = FALSE;
  default_token->marked = FALSE;
  default_token->was_relexed = FALSE;
}

static char *
gpf_default_token_lexeme (GpfToken *token, GpfVersion version)
{
  GpfNode *node = GPF_NODE (token);
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  return gpf_vobject_get (default_token->lexeme, char *, node->document->gvt, version);
}

static int
gpf_default_token_get_state (GpfToken *token, GpfVersion version)
{
  GpfNode *node = GPF_NODE (token);
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  return gpf_vobject_get (default_token->lexer_state, int, node->document->gvt, version);
}

static void
gpf_default_token_set_state (GpfToken *token, int state)
{
  GpfNode *node = GPF_NODE (token);
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  gpf_vobject_set (default_token->state, node->document->gvt, &state, sizeof (int));
}

static int
gpf_default_token_get_lookback (GpfToken *token, GpfVersion version)
{
  GpfNode *node = GPF_NODE (token);
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  return gpf_vobject_get (default_token->lookback, int, node->document->gvt, version);
}

static void
gpf_default_token_set_lookback  (GpfToken *token, int lookback)
{
  GpfNode *node = GPF_NODE (token);
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  gpf_vobject_set (default_token->lookback, node->document->gvt, &lookback, sizeof (int));
}

static int
gpf_default_token_get_lookahead (GpfToken *token, GpfVersion version)
{
  GpfNode *node = GPF_NODE (token);
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  return gpf_vobject_get (default_token->lookahead, int, node->document->gvt, version);
}

static void
gpf_default_token_set_lookahead (GpfToken *token, int lookahead)
{
  GpfNode *node = GPF_NODE (token);
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  gpf_vobject_set (default_token->lookahead, node->document->gvt, &lookahead, sizeof (int));
}

static void
gpf_default_token_mark (GpfToken *token)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  default_token->marked = TRUE;
}

static gboolean
gpf_default_token_is_marked (GpfToken *token)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  return default_token->marked;
}

static void
gpf_default_token_set_re_lexed (GpfToken *token)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  default_token->was_relexed = TRUE;
}

static gboolean
gpf_default_token_was_re_lexed (GpfToken *token)
{
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);

  return default_token->was_relexed;
}

static void
set_length (GpfNode *node, int delta)
{
  int length = gpf_node_get_length (node, GPF_CURRENT_VERSION) + delta;
  gpf_node_set_length (node, length);
  node = gpf_node_get_parent (node, GPF_CURRENT_VERSION);
  if (node == NULL)
    return;
  set_length (node, delta);
}

static void
gpf_default_token_insert_text (GpfToken *token, int offset,
                               const char *text, int length)
{
  GpfNode *node = GPF_NODE (token);
  GpfNode *parent;
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);
  char *old_text;
  char *new_text;
  int old_length;

  if (length < 0)
    length = strlen (text);

  parent = gpf_node_get_parent (GPF_NODE(token), GPF_CURRENT_VERSION);
  if (parent) {
    set_length (parent, length);
    if (!default_token->changed)
      gpf_node_set_nested_changes (parent);
  }

  default_token->changed = TRUE;
  default_token->text_changes = TRUE;

  old_text = gpf_vobject_get (default_token->lexeme, char *, node->document->gvt, GPF_CURRENT_VERSION);
  old_length = strlen (old_text);
  new_text = g_new (char, old_length+length+1);
  new_text [old_length+length] = '\0';
  memcpy (new_text, old_text, offset);
  memcpy (new_text+offset, text, length);
  memcpy (new_text+offset+length, old_text+offset, old_length-offset);
  if (default_token->lexeme->changed)
    g_free (old_text);
  gpf_vobject_set (default_token->lexeme, node->document->gvt, &new_text, sizeof (char *));
}

static void
gpf_default_token_delete_text (GpfToken *token, int start, int end)
{
  GpfNode *node = GPF_NODE (token);
  GpfNode *parent;
  GpfDefaultToken *default_token = GPF_DEFAULT_TOKEN (token);
  char *old_text;
  char *new_text;
  int length = end - start;
  int old_length;

  parent = gpf_node_get_parent (GPF_NODE(token), GPF_CURRENT_VERSION);
  if (parent) {
    set_length (parent, -length);
    if (!default_token->changed)
      gpf_node_set_nested_changes (parent);
  }

  default_token->changed = TRUE;
  default_token->text_changes = TRUE;

  old_text = gpf_vobject_get (default_token->lexeme, char *, node->document->gvt, GPF_CURRENT_VERSION);
  old_length = strlen (old_text);

  length = end - start;

  new_text = g_new (char, old_length-length+1);
  new_text [old_length-length] = '\0';
  memcpy (new_text, old_text, start);
  memcpy (new_text+start, old_text+end, old_length-end);
  if (default_token->lexeme->changed)
    g_free (old_text);
  gpf_vobject_set (default_token->lexeme, node->document->gvt, &new_text, sizeof (char *));
}
