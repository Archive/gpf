/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __GPF_DOCUMENT_H__
#define __GPF_DOCUMENT_H__

#include <time.h>
#include <glib.h>
#include <gtk/gtk.h>
#include "version.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GPF_TYPE_DOCUMENT (gpf_document_get_type ())
#define GPF_DOCUMENT(obj) (GTK_CHECK_CAST (obj, gpf_document_get_type (), GpfDocument))
#define GPF_DOCUMENT_CLASS(klass) \
  (GTK_CHECK_CLASS_CAST (klass, gpf_document_get_type (), GpfDocumentClass))
#define GPF_IS_DOCUMENT(obj) (GTK_CHECK_TYPE (obj, gpf_document_get_type ()))
#define GPF_IS_DOCUMENT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPF_TYPE_DOCUMENT))

/* sentinel nodes */
#define GPF_BOS 0
#define GPF_EOS 0
#define GPF_ULTRA_ROOT -3

/* versioning macros */
#define GPF_REFERENCE_VERSION -3

typedef struct {
  gboolean is_consistent;
  GData *data;
}GpfVersionInfo;

/* GpfDocument */
typedef struct {
  GtkObject parent_object;

  /* file info */
  char *path;
  time_t mtime;

  /* references */
  GHashTable *required_documents;

  /* AST sentinal nodes */
  struct _GpfNode *ultra_root;
  struct _GpfToken *bos;
  struct _GpfToken *eos;

  /* global versioning tree */
  GpfGvt *gvt;
  GpfVersion reference_version;

  /* document text */
  GList *text_changes;
  GpfDiffVObject *buffer_changes;

  /* version info */
  GPtrArray *version_info;

  guint editing  : 1;
  guint pending_close : 1;
}GpfDocument;

typedef struct {
  GtkObjectClass parent_object;
/*
  void (*insert_text) (GpfDocument *document,
                       int          position,
                       const char  *text,
                       int          length);
  void (*delete_text) (GpfDocument *document,
                       int          start,
                       int          end);
*/
  void (*changed)     (GpfDocument *document,
                       GpfVersion   parent_version,
                       GpfVersion   version);
}GpfDocumentClass;

GtkType      gpf_document_get_type    (void);
GpfDocument *gpf_document_new         (void);
GpfDocument *gpf_document_open        (const char  *path);
void         gpf_document_close       (GpfDocument *document);
void         gpf_document_save        (GpfDocument *document);
void         gpf_document_save_as     (GpfDocument *document,
                                       const char  *path);
gboolean     gpf_document_require     (GpfDocument *document,
                                       const char  *path);
void         gpf_document_unrequire   (GpfDocument *document,
                                       const char  *path);
void         gpf_document_begin_edit  (GpfDocument *document);
GpfVersion   gpf_document_end_edit    (GpfDocument *document);
void         gpf_document_set_version (GpfDocument *document,
                                       GpfVersion   version);
int          gpf_document_update      (GpfDocument *document);
void         gpf_document_insert_text (GpfDocument *document,
                                       int          position,
                                       const char  *text,
                                       int          length);
void         gpf_document_delete_text (GpfDocument *document,
                                       int          start_pos,
                                       int          end_pos);
GpfVersionInfo *
             gpf_document_get_version_info (GpfDocument *document,
                                            GpfVersion version);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GPF_DOCUMENT_H__ */
