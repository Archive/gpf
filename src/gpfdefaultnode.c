/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "gpfdefaultnode.h"

/* node states */
#define GPF_NODE_NEW     0
#define GPF_NODE_NORMAL  1
#define GPF_NODE_DELETED 1

static void     gpf_default_node_base_class_init     (GpfDefaultNodeClass *class);
static void     gpf_default_node_base_class_finalize (GpfDefaultNodeClass *class);
static void     gpf_default_node_class_init          (GpfDefaultNodeClass *class);
static void     gpf_default_node_init                (GpfDefaultNode      *node);
static int      gpf_default_node_get_symbol          (GpfNode     *node,
                                                      GpfVersion   version);
static void     gpf_default_node_set_symbol          (GpfNode     *node,
                                                      int          symbol);
static int      gpf_default_node_arity               (GpfNode     *node,
                                                      GpfVersion version);
static int      gpf_default_node_get_length          (GpfNode     *node,
                                                      GpfVersion   version);
static void     gpf_default_node_set_length          (GpfNode     *node,
                                                      int          length);
static GpfNode *gpf_default_node_get_parent          (GpfNode     *node,
                                                      GpfVersion   version);
static void     gpf_default_node_set_parent          (GpfNode     *node,
                                                      GpfNode     *parent);
static GpfNode *gpf_default_node_get_child           (GpfNode     *node,
                                                      int          i,
                                                      GpfVersion   version);
static void     gpf_default_node_set_child           (GpfNode     *node,
                                                      int          i,
                                                      GpfNode     *child);
static gboolean gpf_default_node_has_children        (GpfNode     *node,
                                                      GpfVersion   version);
static int      gpf_default_node_has_child           (GpfNode     *node,
                                                      GpfNode     *child,
                                                      GpfVersion   version);
static gboolean gpf_default_node_has_changes         (GpfNode     *node,
                                                      int          type,
                                                      GpfVersion   version);
static gboolean gpf_default_node_is_new              (GpfNode     *node);
static gboolean gpf_default_node_exists              (GpfNode     *node,
                                                      GpfVersion   version);
static void     gpf_default_node_discard_changes     (GpfNode     *node,
                                                      gboolean     and_nested);
static void     gpf_default_node_set_nested_changes  (GpfNode     *node);
static void     gpf_default_node_set_version         (GpfNode     *node,
                                                      GpfVersion   version);
static void     gpf_default_node_mark_deleted        (GpfNode     *node,
                                                      gboolean     and_nested);
static void     gpf_default_node_undelete            (GpfNode     *node,
                                                      gboolean     and_nested);
static void     gpf_default_node_commit              (GpfNode     *node,
                                                      gboolean     and_nested);

static gboolean gpf_nested_changes_over (GpfNode *node,
                                         GpfVersion from, GpfVersion to);
#if 0
GType
gpf_default_node_get_type ()
{
  static GType type;

  if (!type) {
    static GTypeInfo info = {
      sizeof (GpfDefaultNodeClass),
      (GBaseInitFunc) gpf_default_node_base_class_init,
      (GBaseFinalizeFunc) gpf_default_node_base_class_finalize,
      (GClassInitFunc) gpf_default_node_class_init,
      NULL	/* class_destroy */,
      NULL	/* class_data */,
      sizeof (GpfDefaultNode),
      0		/* n_preallocs */,
      (GInstanceInitFunc) gpf_default_node_init ,
    };

    type = g_type_register_static (gpf_node_get_type (), "GpfDefaultNode", &info);
  }

  return type;
}
#endif

GtkType
gpf_default_node_get_type ()
{
  static int type;

  if (!type) {
    GtkTypeInfo info = {
	   "GpfDefaultNode",
	   sizeof (GpfDefaultNode),
	   sizeof (GpfDefaultNodeClass),
	   (GtkClassInitFunc) gpf_default_node_class_init,
	   (GtkObjectInitFunc) gpf_default_node_init,
	   (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };

    type = gtk_type_unique (gpf_node_get_type (), &info);
  }

  return type;
}

static void
gpf_default_node_base_class_init (GpfDefaultNodeClass *class)
{
  GpfNodeClass *node_class = GPF_NODE_CLASS (class);

  node_class->get_symbol = gpf_default_node_get_symbol;
  node_class->set_symbol = gpf_default_node_set_symbol;
  node_class->arity = gpf_default_node_arity;
  node_class->get_length = gpf_default_node_get_length;
  node_class->set_length = gpf_default_node_set_length;
  node_class->get_parent = gpf_default_node_get_parent;
  node_class->set_parent = gpf_default_node_set_parent;
  node_class->get_child = gpf_default_node_get_child;
  node_class->set_child = gpf_default_node_set_child;
  node_class->has_children = gpf_default_node_has_children;
  node_class->has_child = gpf_default_node_has_child;
  node_class->has_changes = gpf_default_node_has_changes;
  node_class->is_new = gpf_default_node_is_new;
  node_class->exists = gpf_default_node_exists;
  node_class->discard_changes = gpf_default_node_discard_changes;
  node_class->set_nested_changes = gpf_default_node_set_nested_changes;
  node_class->set_version = gpf_default_node_set_version;
  node_class->mark_deleted = gpf_default_node_mark_deleted;
  node_class->undelete = gpf_default_node_undelete;
  node_class->commit = gpf_default_node_commit;
}

static void
gpf_default_node_base_class_finalize (GpfDefaultNodeClass *class)
{
}

static void
gpf_default_node_class_init (GpfDefaultNodeClass *class)
{
  GpfNodeClass *node_class = GPF_NODE_CLASS (class);

  node_class->get_symbol = gpf_default_node_get_symbol;
  node_class->set_symbol = gpf_default_node_set_symbol;
  node_class->arity = gpf_default_node_arity;
  node_class->get_length = gpf_default_node_get_length;
  node_class->set_length = gpf_default_node_set_length;
  node_class->get_parent = gpf_default_node_get_parent;
  node_class->set_parent = gpf_default_node_set_parent;
  node_class->get_child = gpf_default_node_get_child;
  node_class->set_child = gpf_default_node_set_child;
  node_class->has_children = gpf_default_node_has_children;
  node_class->has_child = gpf_default_node_has_child;
  node_class->has_changes = gpf_default_node_has_changes;
  node_class->is_new = gpf_default_node_is_new;
  node_class->exists = gpf_default_node_exists;
  node_class->discard_changes = gpf_default_node_discard_changes;
  node_class->set_nested_changes = gpf_default_node_set_nested_changes;
  node_class->set_version = gpf_default_node_set_version;
  node_class->mark_deleted = gpf_default_node_mark_deleted;
  node_class->undelete = gpf_default_node_undelete;
  node_class->commit = gpf_default_node_commit;
}

static void
gpf_default_node_init (GpfDefaultNode *node)
{
  node->symbol = gpf_vobject_new ();
  node->arity = gpf_vobject_new ();
  node->length = gpf_vobject_new ();
  node->parent = gpf_vobject_new ();
  node->children = gpf_vobject_new ();
  node->state = gpf_vobject_new ();
  node->nested_changes = gpf_vboolean_new ();
}

GpfNode *
gpf_default_node_new (int symbol, GpfNode *parent, GpfNode **children,
                      int n, GpfDocument *document)
{
#if 0
  GpfDefaultNode *node = GPF_DEFAULT_NODE (g_type_create_instance (gpf_default_node_get_type ()));
#endif
  GpfDefaultNode *node = GPF_DEFAULT_NODE (gtk_type_new (gpf_default_node_get_type ()));
  GpfNode *tnode = GPF_NODE (node);
  int state = GPF_NODE_NEW;
  int i, length = 0;

  tnode->document = document;

  gpf_vobject_set (node->symbol, document->gvt, &symbol, sizeof (int));
  gpf_vobject_set (node->arity, document->gvt, &n, sizeof (int));
  gpf_vobject_set (node->parent, document->gvt, &parent, sizeof (GpfNode *));
  gpf_vobject_set (node->children, document->gvt, &children, sizeof (GpfNode **));
  gpf_vobject_set (node->state, document->gvt, &state, sizeof (int));
  gpf_vboolean_set (node->nested_changes, document->gvt, FALSE);

  for (i = 0; i < n; i++) {
    if (children[i]) {
      gpf_node_set_parent (children[i], GPF_NODE (node));
      length += gpf_node_get_length (children[i], GPF_CURRENT_VERSION);
    }
  }

  gpf_vobject_set (node->length, document->gvt, &length, sizeof (int));

  node->changed = TRUE;

  return GPF_NODE (node);
}

static int
gpf_default_node_get_symbol (GpfNode *node, GpfVersion version)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);

  return gpf_vobject_get (default_node->symbol, int, node->document->gvt, version);
}

static void
gpf_default_node_set_symbol (GpfNode *node, int symbol)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);
  g_assert (node->document->editing);

  gpf_vobject_set (default_node->symbol, node->document->gvt, &symbol, sizeof (int));
}

static int
gpf_default_node_arity (GpfNode *node, GpfVersion version)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);

  return gpf_vobject_get (default_node->arity, int, node->document->gvt, version);
}

static int
gpf_default_node_get_length (GpfNode *node, GpfVersion version)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);

  return gpf_vobject_get (default_node->length, int, node->document->gvt, version);
}

static void
gpf_default_node_set_length (GpfNode *node, int length)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);
  g_assert (node->document->editing);

  gpf_vobject_set (default_node->length, node->document->gvt, &length, sizeof (int));
}

static GpfNode *
gpf_default_node_get_parent (GpfNode *node, GpfVersion version)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);

  return gpf_vobject_get (default_node->parent, GpfNode *, node->document->gvt, version);
}

static void
gpf_default_node_set_parent (GpfNode *node, GpfNode *parent)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);
  g_assert (node->document->editing);

  gpf_vobject_set (default_node->parent, node->document->gvt, &parent, sizeof (GpfNode *));
}

static GpfNode *
gpf_default_node_get_child (GpfNode *node, int i, GpfVersion version)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);

  if (i >= gpf_vobject_get (default_node->arity, int, node->document->gvt, version))
    return NULL;

  return (gpf_vobject_get (default_node->children, GpfNode **, node->document->gvt, version))[i];
}

static void
gpf_default_node_set_child (GpfNode *node, int i,
                            GpfNode *child)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);
  int arity;
  GpfNode **children;
  g_assert (node->document->editing);

  arity = gpf_vobject_get (default_node->arity, int, node->document->gvt, GPF_CURRENT_VERSION);

  if (i > arity)
    return;

  children = gpf_vobject_get (default_node->children, GpfNode **, node->document->gvt, GPF_CURRENT_VERSION);

  if (gpf_vobject_changed (default_node->children)) {
    if (i == arity) {
      arity++;
      children = g_realloc (children, sizeof (GpfNode *) * arity);
    }
  } else {
    GpfNode **new_children;
    int old_arity = arity;

    if (i == arity) {
      arity++;
      gpf_vobject_set (default_node->arity, node->document->gvt, &arity, sizeof (int));
    }

    new_children = g_malloc (sizeof (GpfNode *) * arity);
    memcpy (new_children, children, sizeof (GpfNode *) * old_arity);

    children = new_children;
  }

  children[i] = child;
  gpf_vobject_set (default_node->children, node->document->gvt, &children, sizeof (GpfNode **));
  gpf_node_set_parent (child, node);
}

static gboolean
gpf_default_node_has_children (GpfNode *node, GpfVersion version)
{
  return gpf_default_node_arity (node, version);
}

static int
gpf_default_node_has_child (GpfNode *node, GpfNode *child, GpfVersion version)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);
  GpfNode **children;
  int i, arity;

  children = gpf_vobject_get (default_node->children, GpfNode **, node->document->gvt, version);
  arity = gpf_vobject_get (default_node->arity, int, node->document->gvt, version);

  for (i = 0; i < arity; i++) {
    if (child == children[i])
      return i;
  }

  return -1;
}

static gboolean
gpf_default_node_has_changes (GpfNode *node, int type, GpfVersion version)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);

  if (version == GPF_CURRENT_VERSION) {
    if (type & GPF_LOCAL_CHANGES)
      if (default_node->changed)
        return TRUE;
    if (type & GPF_NESTED_CHANGES)
      if (gpf_vboolean_changed (default_node->nested_changes))
        return TRUE;
    if (type & GPF_CHILD_CHANGES)
      if (default_node->child_changes)
        return TRUE;
  } else {
    if (type & GPF_LOCAL_CHANGES)
      if (gpf_vobject_changed_over (default_node->symbol, node->document->gvt, node->document->gvt->global_version, version)
          || gpf_vobject_changed_over (default_node->length, node->document->gvt, node->document->gvt->global_version, version)
          || gpf_vobject_changed_over (default_node->parent, node->document->gvt, node->document->gvt->global_version, version)
          || gpf_vobject_changed_over (default_node->children, node->document->gvt, node->document->gvt->global_version, version)
          || gpf_vobject_changed_over (default_node->state, node->document->gvt, node->document->gvt->global_version, version))
        return TRUE;
    if (type & GPF_NESTED_CHANGES)
      if (gpf_nested_changes_over (node, node->document->gvt->global_version, version))
        return TRUE;
    if (type & GPF_CHILD_CHANGES)
      if (gpf_vobject_changed_over (default_node->children, node->document->gvt, node->document->gvt->global_version, version))
        return TRUE;
  }

  return FALSE;
}

static gboolean
gpf_default_node_is_new (GpfNode *node)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);

  if (default_node->state->vlog[0] == node->document->gvt->global_version)
    return TRUE;

  return FALSE;
}

static gboolean
gpf_default_node_exists (GpfNode *node, GpfVersion version)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);

  return (gpf_vobject_get (default_node->state, int, node->document->gvt, version) != GPF_NODE_DELETED);
}

static void
gpf_default_node_discard_changes (GpfNode *node, gboolean and_nested)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);

  if (default_node->nested_changes && and_nested) {
    GpfNode *child;
    int i;

    for (i = 0; i < gpf_default_node_arity (node, GPF_CURRENT_VERSION); i++) {
      child = gpf_default_node_get_child (node, i, GPF_CURRENT_VERSION);
      if (gpf_node_has_changes (child, GPF_LOCAL_CHANGES, GPF_CURRENT_VERSION))
        gpf_node_discard_changes (child, TRUE);
    }
  }

  if (default_node->changed) {
    gpf_vobject_discard_changes (default_node->symbol);
    gpf_vobject_discard_changes (default_node->arity);
    gpf_vobject_discard_changes (default_node->length);
    gpf_vobject_discard_changes (default_node->parent);
    gpf_vobject_discard_changes (default_node->children);
    gpf_vobject_discard_changes (default_node->state);
  }
}

static void
gpf_default_node_set_nested_changes (GpfNode *node)
{
  GpfDefaultNode *default_node;

  if (node == NULL)
    return;

  default_node = GPF_DEFAULT_NODE (node);
  gpf_vboolean_set (default_node->nested_changes, node->document->gvt, TRUE);
  node = gpf_node_get_parent (node, GPF_CURRENT_VERSION);

  if (node == NULL)
    return;

  gpf_node_set_nested_changes (node);
}

static void
gpf_default_node_set_version (GpfNode *node, GpfVersion version)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);

  gpf_vobject_set_version (default_node->symbol, node->document->gvt, version);
  gpf_vobject_set_version (default_node->arity, node->document->gvt, version);
  gpf_vobject_set_version (default_node->length, node->document->gvt, version);
  gpf_vobject_set_version (default_node->parent, node->document->gvt, version);
  gpf_vobject_set_version (default_node->children, node->document->gvt, version);
  gpf_vobject_set_version (default_node->state, node->document->gvt, version);
}

static void
gpf_default_node_mark_deleted (GpfNode *node, gboolean and_nested)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);
  int state = GPF_NODE_DELETED;

  gpf_vobject_set (default_node->state, node->document->gvt, &state, sizeof (int));
}

static void
gpf_default_node_undelete (GpfNode *node, gboolean and_nested)
{
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);
  if (gpf_vobject_changed (default_node->state))
    gpf_vobject_discard_changes (default_node->state);
}

static void
gpf_default_node_commit (GpfNode *node, gboolean and_nested)
{
  GpfNode *child;
  GpfDefaultNode *default_node = GPF_DEFAULT_NODE (node);
  int i;

  if (and_nested && gpf_vboolean_changed (default_node->nested_changes)) {
    gpf_vboolean_commit (default_node->nested_changes);
    for (i = 0; i < gpf_default_node_arity (node, GPF_CURRENT_VERSION); i++) {
      child = gpf_default_node_get_child (node, i, GPF_CURRENT_VERSION);
      if (child && gpf_node_has_changes (child, GPF_LOCAL_CHANGES,
                                         GPF_CURRENT_VERSION))
        gpf_node_commit (child, TRUE);
    }
  }

  if (default_node->changed) {
    default_node->changed = FALSE;
    default_node->child_changes = FALSE;
    gpf_vobject_commit (default_node->symbol);
    gpf_vobject_commit (default_node->arity);
    gpf_vobject_commit (default_node->length);
    gpf_vobject_commit (default_node->parent);
    gpf_vobject_commit (default_node->children);
    gpf_vobject_commit (default_node->state);
  }
}

static gboolean
gpf_nested_changes_over (GpfNode *node, GpfVersion from, GpfVersion to)
{
  GpfGvt *gvt = node->document->gvt;
  GpfVBoolean *nested_changes = GPF_DEFAULT_NODE (node)->nested_changes;
  GpfGvtNode *from_gd = g_ptr_array_index (gvt->version_node_map, from);
  GpfGvtNode *to_gd = g_ptr_array_index (gvt->version_node_map, to);
  int lvid = -1;

  g_assert(from >= 0 && from < gvt->version_node_map->len);
  g_assert(to >= 0 && to < gvt->version_node_map->len);

  if (from == to)
    return FALSE;

  if (from_gd == to_gd) {
    /* Common (and easy!) case: source and target in same linear run. */
    if (from > to) {
      for (lvid = gpf_vboolean_project (nested_changes, gvt, from);
           nested_changes->vlog[lvid] > to; lvid--)
        if (gpf_node_exists (node, nested_changes->vlog[lvid]))
          return TRUE;
    } else if (to > from) {
      for (lvid = gpf_vboolean_project (nested_changes, gvt, from+1);
           lvid < nested_changes->len
           && nested_changes->vlog[lvid] > from
           && nested_changes->vlog[lvid] <= to; ++lvid)
        if (gpf_node_exists (node, nested_changes->vlog[lvid]))
          return TRUE;
    }
  } else {
    /* Uncommon (and hard) case: perform a general search. */
    GpfGvtNode *gd = from_gd;
    GpfGvtNode *lca;
    if (gpf_gvt_ancestor(gd, to_gd) && from + 1 < gvt->version_node_map->len) {
      /* Handle suffix in starting gd. */
      for (lvid = gpf_vboolean_project (nested_changes, gvt, from + 1);
           lvid < nested_changes->len && nested_changes->vlog[lvid] > from &&
           nested_changes->vlog[lvid] <= gd->end_version;
           ++lvid)
        if (gpf_node_exists (node, nested_changes->vlog[lvid]))
          return TRUE;
      /* The LCA problem: either the penultimate gvid is inside 'gd',
         in which case we processed it in the above loop, or it's the
         starting gvid in the child that's going to be the penultimate
         gd on the redo path, which we take care of below. */
    } else {
      gboolean need_proj = TRUE;
      GpfGvtNode *last_gd = gd;

      /* Handle prefix in starting gd. */
      for (lvid = gpf_vboolean_project (nested_changes, gvt, from);
           nested_changes->vlog[lvid] >= from_gd->start_version &&
           nested_changes->vlog[lvid] <= from_gd->end_version;
           lvid--)
        if (gpf_node_exists (node, nested_changes->vlog[lvid]))
          return TRUE;

      gd = from_gd->parent;
      while (!gpf_gvt_ancestor(gd, to_gd)) {
        if (need_proj)
          lvid = gpf_vboolean_project (nested_changes, gvt, gd->end_version);
        for (; nested_changes->vlog[lvid] >= gd->start_version &&
               nested_changes->vlog[lvid] <= gd->end_version; lvid--)
          if (gpf_node_exists (node, nested_changes->vlog[lvid]))
            return TRUE;

        need_proj = gd->parent->children != gd;
        last_gd = gd;
        gd = gd->parent;
      }
      /* The penultimate gvid on the undo side, if it's not inside a
         GD, must be the starting gvid of the penultimate GD on this
         path. So project that explicitly. */
      if (gpf_node_exists (node, last_gd->start_version))
        return TRUE;
    }

    lca = gd;
    /* Now we process the redo side in a symmetric fashion. The only
       difference is that we don't need to do ancestor computations,
       since we already know what the LCA is. */
    gd = to_gd;
    if (gpf_gvt_ancestor(gd, from_gd) && to + 1 < gvt->version_node_map->len) {
      /* Handle suffix in ending gd. */
      for (lvid = gpf_vboolean_project (nested_changes, gvt, to+1);
           lvid < nested_changes->len 
           && nested_changes->vlog[lvid] <= gd->end_version
           && nested_changes->vlog[lvid] > to; ++lvid)
        if (gpf_node_exists (node, nested_changes->vlog[lvid]))
          return TRUE;

      /* The LCA problem: either the penultimate gvid is inside 'gd',
         in which case we processed it in the above loop, or it's the
         starting gvid in the child that's going from be the
         penultimate gd on the redo path, which we take care of
         below. */
    } else {
      gboolean need_proj;
      GpfGvtNode *last_gd;
      /* Handle prefix in ending gd. */
      for (lvid = gpf_vboolean_project (nested_changes, gvt, to);
           nested_changes->vlog[lvid] >= to_gd->start_version
           && nested_changes->vlog[lvid] <= to_gd->end_version;
           lvid--)
        if (gpf_node_exists (node, nested_changes->vlog[lvid]))
          return TRUE;

      need_proj = TRUE;
      last_gd = gd;
      gd = to_gd->parent;
      while (gd != lca) {
        if (need_proj)
          lvid = gpf_vboolean_project (nested_changes, gvt, gd->end_version);
        for (;
             nested_changes->vlog[lvid] >= gd->start_version
             && nested_changes->vlog[lvid] <= gd->end_version;
             lvid--)
          if (gpf_node_exists (node, nested_changes->vlog[lvid]))
            return TRUE;
        need_proj = gd->parent->children != gd;
        last_gd = gd;
        gd = gd->parent;
      }
      /* The penultimate gvid on the redo side, if it's not inside a
         GD, must be the starting gvid of the penultimate GD on this
         path. So project that explicitly. */
      if (gpf_node_exists (node, nested_changes->vlog[lvid]))
        return TRUE;
    }
  }

  /* No matching value found. */
  return FALSE;
}
