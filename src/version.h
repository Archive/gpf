/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __GPF_VERSION_H__
#define __GPF_VERSION_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define gpf_gvt_get_node(gvt, version) \
  (g_ptr_array_index (gvt->version_node_map, version))

#define gpf_gvt_ancestor(a, b)  \
  (((a)->preorder_index < (b)->preorder_index) && ((a)->postorder_index > (b)->postorder_index))

#define gpf_vobject_index(vobject, type, index) ((type*)(vobject)->log)[(index)])

#define gpf_vobject_get(vobject, type, gvt, version)                               \
(                                                                                  \
  ((version) == GPF_CURRENT_VERSION)? (                                            \
    ((type*)(vobject)->log)[(vobject)->log_index]                                  \
  ) : (                                                                            \
    ((version) == GPF_PREVIOUS_VERSION)? (                                         \
      ((vobject)->changed)?                                                        \
        ((type*)(vobject)->log)[(vobject)->log_index-1]                            \
      :                                                                            \
        ((type*)(vobject)->log)[(vobject)->log_index]                              \
    ) :                                                                            \
      (((type*)(vobject)->log)[gpf_vobject_project ((vobject), (gvt), (version))]) \
  )                                                                                \
)

#define gpf_vboolean_project(vboolean, gvt, version) \
  gpf_vobject_project ((GpfVObject *)vboolean, gvt, version)

#define gpf_vboolean_index(vboolean, index) \
  ((vboolean)->log[index / 8] & (1 << (index % 8)))

#define gpf_vboolean_changed(vboolean) \
  ((vboolean)->log && (vboolean)->log[(vboolean)->len / 8] & 0x80)

#define GPF_PREVIOUS_VERSION -2
#define GPF_CURRENT_VERSION -1

typedef int GpfVersion;

/* GpfVObject */
struct _GpfVObject {
  int         log_index;
  guint8     *log;
  GpfVersion *vlog;
  guint       len;
  /* change fields */
  unsigned int changed : 1;
};

typedef struct _GpfDiffVObject {
  int         log_index;
  guint8     *log;
  GpfVersion *vlog;
  guint       len;
  /* change fields */
  unsigned int changed : 1;
}GpfDiffVObject;

typedef struct _GpfVBoolean {
  int         log_index;
  guint8     *log;
  GpfVersion *vlog;
  guint       len;
}GpfVBoolean;

typedef struct _GpfVObject GpfVObject;
typedef struct _GpfGvtNode GpfGvtNode;

/* GpfGvtNode */
struct _GpfGvtNode {
  GData *data;
  int preorder_index;
  int postorder_index;
  int start_version;
  int end_version;
  struct _GpfGvtNode *parent;
  struct _GpfGvtNode *children;
  struct _GpfGvtNode *prev;
  struct _GpfGvtNode *next;
};

/* GpfGvt */
typedef struct _GpfGvt {
  GpfGvtNode *root;
  GPtrArray *version_node_map;
  GArray *preorder_map;
  GArray *postorder_map;
  int global_version;
}GpfGvt;

/* -- gpf global versioning tree -- */
GpfGvt *gpf_gvt_new              (void);
void    gpf_gvt_free             (GpfGvt *gvt);
int     gpf_gvt_new_version      (GpfGvt *gvt);
int     gpf_gvt_last_version     (GpfGvt *gvt);
int     gpf_gvt_preorder_compare (GpfGvtNode *a, GpfGvtNode *b);

/* -- gpf versioned boolean -- */
GpfVBoolean *gpf_vboolean_new             ();
void         gpf_vboolean_free            (GpfVBoolean *vboolean);
gboolean     gpf_vboolean_get             (GpfVBoolean *vboolean,
                                           GpfGvt *gvt,
                                           GpfVersion version);
void         gpf_vboolean_set             (GpfVBoolean *vboolean,
                                           GpfGvt      *gvt,
                                           gboolean     value);
void         gpf_vboolean_commit          (GpfVBoolean *vboolean);
void         gpf_vboolean_discard_changes (GpfVBoolean *vboolean);

/* -- gpf versioned object -- */
GpfVObject *gpf_vobject_new             ();
void        gpf_vobject_free            (GpfVObject *vobject);
int         gpf_vobject_project         (GpfVObject *vobject,
                                         GpfGvt     *gvt,
                                         GpfVersion  version);
void        gpf_vobject_set             (GpfVObject *vobject,
                                         GpfGvt     *gvt,
                                         gpointer    value,
                                         int         elt_size);
void        gpf_vobject_commit          (GpfVObject *vobject);
gboolean    gpf_vobject_changed         (GpfVObject *vobject);
gboolean    gpf_vobject_changed_over    (GpfVObject *vobject,
                                         GpfGvt     *gvt,
                                         GpfVersion  version_from,
                                         GpfVersion  version_to);
gboolean    gpf_vobject_set_version     (GpfVObject *vobject,
                                         GpfGvt     *gvt,
                                         GpfVersion  version);
gboolean    gpf_vobject_has_changes     (GpfVObject *vobject,
                                         GpfGvt     *gvt,
                                         GpfVersion  version);
gboolean    gpf_vobject_had_value       (GpfVObject *vobject,
                                         gpointer    value,
                                         int (*compare) (gpointer a, gpointer b));
void        gpf_vobject_discard_changes (GpfVObject *vobject);

/* -- gpf differential versioned object -- */
GpfDiffVObject *gpf_diff_vobject_new             ();
void            gpf_diff_vobject_free            (GpfDiffVObject *vobject);
gpointer        gpf_diff_vobject_get             (GpfDiffVObject *vobject,
                                                  GpfGvt *gvt,
                                                  GpfVersion version,
                                                  void (*apply)(gpointer value, gpointer delta, gboolean forward));
void            gpf_diff_vobject_set             (GpfDiffVObject *vobject,
                                                  GpfGvt         *gvt,
                                                  gpointer        delta,
                                                  int             elt_size);
void            gpf_diff_vobject_commit          (GpfDiffVObject *vobject,
                                                  void (*apply)(gpointer value, gpointer delta, gboolean forward));
gboolean        gpf_diff_vobject_changed         (GpfDiffVObject *vobject);
gboolean        gpf_diff_vobject_changed_over    (GpfDiffVObject *vobject,
                                                  GpfGvt         *gvt,
                                                  GpfVersion      version_from,
                                                  GpfVersion      version_to);
gboolean        gpf_diff_vobject_set_version     (GpfDiffVObject *vobject,
                                                  GpfGvt         *gvt,
                                                  GpfVersion      version);
gboolean        gpf_diff_vobject_has_changes     (GpfDiffVObject *vobject,
                                                  GpfGvt         *gvt,
                                                  GpfVersion      version);
gboolean        gpf_diff_vobject_had_value       (GpfDiffVObject *vobject,
                                                  gpointer        value,
                                                  int (*compare) (const gpointer a, const gpointer b));
void            gpf_diff_vobject_discard_changes (GpfDiffVObject *vobject);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GPF_VERSION_H__ */
