/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "gpfdocument.h"
#include "gpfdefaulttoken.h"
#include "gpfdefaultnode.h"
#include "parser.h"

static GtkObjectClass *parent_class = NULL;
static GHashTable *document_hash;
static GMainLoop *event_loop;

static void process_deletions (GpfDocument *document, GpfNode *node);

/* widget stuff */
static void gpf_document_class_init (GpfDocumentClass *klass);
static void gpf_document_init       (GpfDocument      *document);
static void gpf_document_destroy    (GtkObject        *object);

/* --<widget initialization, constructor and destructor>------------------ */
GtkType
gpf_document_get_type ()
{
  static guint document_type = 0;

  if (!document_type) {
    GtkTypeInfo document_info = {
	   "GpfDocument",
	   sizeof (GpfDocument),
	   sizeof (GpfDocumentClass),
	   (GtkClassInitFunc) gpf_document_class_init,
	   (GtkObjectInitFunc) gpf_document_init,
	   (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };

    document_type = gtk_type_unique (gtk_object_get_type (), &document_info);
  }

  return document_type;
}

static int changed_signal;

static void
gpf_document_class_init (GpfDocumentClass *class)
{
  GtkObjectClass *object_class = (GtkObjectClass*) class;

  parent_class = gtk_type_class (gtk_object_get_type ());

  changed_signal = gtk_signal_new ("changed",
                                   GTK_RUN_FIRST,
                                   object_class->type,
                                   GTK_SIGNAL_OFFSET (GpfDocumentClass, changed),
                                   gtk_marshal_NONE__INT_INT,
                                   GTK_TYPE_NONE,
                                   2,
                                   GTK_TYPE_INT, GTK_TYPE_INT);

  gtk_object_class_add_signals (object_class, &changed_signal, 1);

  object_class->destroy = gpf_document_destroy;
}

static void
gpf_document_init (GpfDocument *document)
{
  GpfNode **children;
  GpfVersionInfo *info;

  /* file info */
  document->path = NULL;
  document->mtime = -1;

  /* references */
  document->required_documents = g_hash_table_new (g_str_hash, NULL);

  /* Initialize the global versioning tree. */
  document->gvt = gpf_gvt_new ();
  document->reference_version = 0;

  /* Initialize version info. */
  document->version_info = g_ptr_array_new ();
  info = g_new (GpfVersionInfo, 1);
  info->is_consistent = FALSE;
  g_datalist_init (&info->data);
  g_ptr_array_add (document->version_info, info);

  /* Initialize the tree */
  gpf_document_begin_edit (document);
  document->bos = gpf_default_token_new (GPF_BOS, "", NULL, 0, 0, 0, document);
  document->eos = gpf_default_token_new (GPF_EOS, "", NULL, 0, 0, 0, document);
  children = g_new (GpfNode *, 3);
  children[0] = GPF_NODE (document->bos);
  children[1] = NULL;
  children[2] = GPF_NODE (document->eos);
  document->ultra_root = gpf_default_node_new (GPF_ULTRA_ROOT, NULL, children, 3, document);

  /* Initialize boolean fields */
  document->pending_close = FALSE;
}

static void
gpf_document_destroy (GtkObject *object)
{
  GpfDocument *document = GPF_DOCUMENT (object);

  g_hash_table_remove (document_hash, document->path);
    
  if (parent_class->destroy)
    parent_class->destroy (object);
}

GpfDocument *
gpf_document_new ()
{
  GpfDocument *document = GPF_DOCUMENT (gtk_type_new (gpf_document_get_type ()));
  gpf_document_end_edit (document);
  /* Emulate GObject */
  gtk_object_ref (GTK_OBJECT (document));
  gtk_object_sink (GTK_OBJECT (document));

  return document;
}

GpfDocument *
gpf_document_open (const char *path)
{
  FILE *file;
  struct stat file_stat;
  GpfDocument *document;
  char *buffer;

  if (document_hash) {
    document = g_hash_table_lookup (document_hash, path);
    if (document) {
      gtk_object_ref (GTK_OBJECT (document));
      return document;
    }
  }

  file = fopen (path, "r");

  if (!file)
    return NULL;

  if (!document_hash)
    document_hash = g_hash_table_new (g_str_hash, NULL);
  stat (path, &file_stat);
  buffer = g_malloc (file_stat.st_size);
  fread (buffer, file_stat.st_size, 1, file);
  fclose (file);
  document = GPF_DOCUMENT (gtk_type_new (gpf_document_get_type ()));
  document->path = strdup (path);
  gpf_document_insert_text (document, 0, buffer, file_stat.st_size);
  gpf_document_end_edit (document);
  g_free (buffer);
  event_loop = g_main_new (FALSE);

  g_hash_table_insert (document_hash, document->path, document);

  return document;
}

void
gpf_document_close (GpfDocument *document)
{
  document->pending_close = TRUE;

  gtk_object_unref (GTK_OBJECT (document));
}

void
gpf_document_save (GpfDocument *document)
{
}

void
gpf_document_save_as (GpfDocument *document, const char *path)
{
}

gboolean
gpf_document_require (GpfDocument *document, const char *path)
{
  if (!g_hash_table_lookup (document->required_documents, path)) {
    GpfDocument *required_document = gpf_document_open (path);
    if (!required_document)
      return FALSE;

    g_hash_table_insert (document->required_documents, required_document->path, required_document);
  }
  return TRUE;
}

void
gpf_document_unrequire (GpfDocument *document, const char *path)
{
  GpfDocument *required_document;
  if ((required_document = g_hash_table_lookup (document->required_documents, path))) {
    gpf_document_close (required_document);
    g_hash_table_remove (document->required_documents, path);
  }
}

void
gpf_document_begin_edit (GpfDocument *document)
{
  document->editing = TRUE;
}

GpfVersion
gpf_document_end_edit (GpfDocument *document)
{
  GpfVersionInfo *info;
  GpfVersion parent_version, new_version;

  if (!document->editing)
    return document->gvt->global_version;

  parent_version = document->gvt->global_version;
  new_version = gpf_gvt_new_version (document->gvt);
  info = g_new (GpfVersionInfo, 1);
  info->is_consistent = FALSE;
  g_datalist_init (&info->data);
  g_ptr_array_add (document->version_info, info);
  gtk_signal_emit (GTK_OBJECT (document), changed_signal,
                   parent_version, new_version);
  process_deletions (document, document->ultra_root);
  gpf_node_commit (document->ultra_root, TRUE);

  return new_version;
}

void
document_set_version (GpfNode *node, GpfVersion version)
{
  if (!node)
    return;

  if (gpf_node_has_changes (node, GPF_NESTED_CHANGES, version)) {
    int i;

    for (i = 0; i < gpf_node_arity (node, version); i++)
      document_set_version (gpf_node_get_child (node, i, version),  version);
  }
  gpf_node_set_version (node, version);
}


void
gpf_document_set_version (GpfDocument *document, GpfVersion version)
{
  if (version == document->gvt->global_version)
    return;
  gtk_signal_emit (GTK_OBJECT (document), changed_signal, 
                   document->gvt->global_version, version);
  document_set_version (document->ultra_root, version);
  document->gvt->global_version = version;
}

int
gpf_document_update (GpfDocument *document)
{
  GpfGvtNode *cur = gpf_gvt_get_node (document->gvt, 
                                      document->gvt->global_version);
  GpfVersion new_version, version = document->gvt->global_version;
  GpfVersionInfo *info = gpf_document_get_version_info (document, version);

  if (info->is_consistent)
    return version;

  if (version != 0) {
    version--;
    while (version != 0) {
      for (; version >= cur->start_version; version--) {
        info = gpf_document_get_version_info (document, version);
        if (info->is_consistent)
          goto out;
      }
      cur = cur->parent;
      version = cur->end_version;
    }
    out:
  }

  document->reference_version = version;
  gpf_inc_parse (document);
  new_version = gpf_gvt_new_version (document->gvt);
  gtk_signal_emit (GTK_OBJECT (document), changed_signal,
                   version, new_version);
  gpf_node_commit (document->ultra_root, TRUE);

  return new_version;
}

GpfToken *
find_nearest_token (GpfNode *node, int reference, int position,
                    int *token_pos)
{
  if (GPF_IS_TOKEN (node)) {
    *token_pos = reference;
    return GPF_TOKEN (node);
  } else {
    GpfNode *child;
    int i, length;

    for (i = 0; i < gpf_node_arity (node, GPF_CURRENT_VERSION); i++) {
      child = gpf_node_get_child (node, i, GPF_CURRENT_VERSION);
      length = gpf_node_get_length (child, GPF_CURRENT_VERSION);

      if (reference < position && position <= reference + length)
        return find_nearest_token (child, reference, position, token_pos);

      reference += length;
    }
  }

  return NULL;
}

void
gpf_document_insert_text (GpfDocument *document, int position,
                          const char *text, int length)
{
  g_assert (document->editing);
  g_assert (position >= 0);
  g_assert (position <= gpf_node_get_length (document->ultra_root, GPF_CURRENT_VERSION));

  if (length == 0)
    return;

  if (length < 0)
    length = strlen (text);

  if (position == 0)
    gpf_token_insert_text (document->bos, 0, text, length);
  else {
    GpfToken *token;
    int token_pos;

    token = find_nearest_token (document->ultra_root, 0, position, &token_pos);
    gpf_token_insert_text (token, position - token_pos, text, length);
  }
}

void
delete_text (GpfNode *root, int start_pos, int end_pos, int pos)
{
  int length = gpf_node_get_length (root, GPF_CURRENT_VERSION);

  if (start_pos <= pos && pos + length <= end_pos)
      gpf_node_mark_deleted (root, TRUE);
  else if (GPF_IS_TOKEN (root)) {
    int start = (start_pos > pos)? start_pos : pos;
    int end = (end_pos > pos + length)? pos + length : end_pos;

    gpf_token_delete_text (GPF_TOKEN(root), start, end);
  } else {
    GpfNode *node;
    int i;

    for (i = 0; i < gpf_node_arity (root, GPF_CURRENT_VERSION); i++) {
      node = gpf_node_get_child (root, i, GPF_CURRENT_VERSION);
      length = gpf_node_get_length (node, GPF_CURRENT_VERSION);

      if (pos >= end_pos)
        break;
      else if (!(pos <= start_pos && pos + length <= start_pos))
        delete_text (node, start_pos, end_pos, pos);

      pos += length;
    }
  }
}

void
gpf_document_delete_text (GpfDocument *document, int start_pos, int end_pos)
{
  int length = gpf_node_get_length(document->ultra_root, GPF_CURRENT_VERSION);

  g_assert (document->editing);
  g_assert (start_pos >= 0);
  g_assert (start_pos <= length);
  g_assert (end_pos <= length);

  if (end_pos < 0)
    end_pos = length;

  if (start_pos == end_pos)
    return;

  delete_text (document->ultra_root, start_pos, end_pos, 0);
}

GpfVersionInfo *
gpf_document_get_version_info (GpfDocument *document, GpfVersion version)
{
  return g_ptr_array_index (document->version_info, version);
}

/* A node is in the current version of the tree if a retraceable path
   to the root exists. */
static gboolean
node_in_current_tree (GpfDocument *document, GpfNode *node)
{
  GpfNode *p;

  if (!gpf_node_exists(node, GPF_CURRENT_VERSION))
    return FALSE;

  for (p = gpf_node_get_parent(node, GPF_CURRENT_VERSION);
       p != document->ultra_root
       && gpf_node_exists(GPF_NODE (p), GPF_CURRENT_VERSION)
       && gpf_node_has_child(p, node, GPF_CURRENT_VERSION);
       p = gpf_node_get_parent(p, GPF_CURRENT_VERSION))
    node = p;
  return node == document->ultra_root;
}

/* Mark contiguous deleted nodes. */
static void
handle_deletion (GpfNode *node, GpfDocument *document)
{
  int i, arity = gpf_node_arity (node, GPF_CURRENT_VERSION);

  gpf_node_mark_deleted(node, FALSE);
  /* Iterate over kids, checking each one for deletion. */
  for (i = 0; i < arity; ++i) {
    GpfNode *child = gpf_node_get_child (node, i, GPF_CURRENT_VERSION);
    if (child && gpf_node_exists(child, GPF_CURRENT_VERSION)) {
      if (gpf_node_get_parent (child, GPF_CURRENT_VERSION) == NULL
          || gpf_node_get_parent (child, GPF_CURRENT_VERSION) == node)
        handle_deletion(child, document);
      else if (!node_in_current_tree(document, child))
        handle_deletion(child, document);
    }
  }
}

/* Find root of each deleted substructure. */
static void
process_deletions (GpfDocument *document, GpfNode *node)
{
  int i;

  if (!node)
    return;

  if (!gpf_node_is_new(node)
      && gpf_node_has_changes(node, GPF_LOCAL_CHANGES, GPF_CURRENT_VERSION)) {
    int arity = gpf_node_arity(node, GPF_CURRENT_VERSION);
    for (i = 0; i < arity; i++) {
      GpfNode *old_kid = gpf_node_get_child(node, i, GPF_PREVIOUS_VERSION);
      if (old_kid && gpf_node_exists(old_kid, GPF_CURRENT_VERSION)
          && !gpf_node_has_child(node, old_kid, GPF_CURRENT_VERSION)
          && (gpf_node_get_parent(old_kid, GPF_CURRENT_VERSION) == NULL 
              || !node_in_current_tree(document, old_kid)))
        handle_deletion(old_kid, document);
    }
  }
  if (gpf_node_has_changes(node, GPF_NESTED_CHANGES, GPF_CURRENT_VERSION))
    for (i = 0; i < gpf_node_arity(node, GPF_CURRENT_VERSION); ++i)
      process_deletions (document, gpf_node_get_child(node, i, GPF_CURRENT_VERSION));
}
