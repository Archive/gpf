/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "gpftoken.h"
#include "common.h"

static void gpf_token_base_class_init (GpfTokenClass *class);
static void gpf_token_base_class_finalize (GpfTokenClass *class);
static void gpf_token_class_init (GpfTokenClass *class);
static void gpf_token_init       (GpfToken      *token);

#if 0
GType
gpf_token_get_type ()
{
  static GType type;

  if (!type) {
    static GTypeInfo info = {
      sizeof (GpfTokenClass),
      (GBaseInitFunc) gpf_token_base_class_init,
      (GBaseFinalizeFunc) gpf_token_base_class_finalize,
      (GClassInitFunc) gpf_token_class_init,
      NULL	/* class_destroy */,
      NULL	/* class_data */,
      sizeof (GpfTokenClass),
      0		/* n_preallocs */,
      (GInstanceInitFunc) gpf_token_init ,
    };

    type = g_type_register_static (G_TYPE_INTERFACE, "GpfToken", &info);
  }

  return type;
}
#endif

GtkType
gpf_token_get_type ()
{
  static int type;

  if (!type) {
    GtkTypeInfo info = {
	   "GpfToken",
	   sizeof (GpfToken),
	   sizeof (GpfTokenClass),
	   (GtkClassInitFunc) gpf_token_class_init,
	   (GtkObjectInitFunc) gpf_token_init,
	   (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };

    type = gtk_type_unique (gpf_node_get_type (), &info);
  }

  return type;
}

static void
gpf_token_base_class_init (GpfTokenClass *class)
{
  class->lexeme = NULL;
  class->get_state = NULL;
  class->set_state = NULL;
  class->get_lookback = NULL;
  class->set_lookback = NULL;
  class->get_lookahead = NULL;
  class->set_lookahead = NULL;
  class->mark = NULL;
  class->is_marked = NULL;
  class->set_re_lexed = NULL;
  class->was_re_lexed = NULL;
  class->insert_text = NULL;
  class->delete_text = NULL;
}

static void
gpf_token_base_class_finalize (GpfTokenClass *class)
{
}

static void
gpf_token_class_init (GpfTokenClass *class)
{
  class->lexeme = NULL;
  class->get_state = NULL;
  class->set_state = NULL;
  class->get_lookback = NULL;
  class->set_lookback = NULL;
  class->get_lookahead = NULL;
  class->set_lookahead = NULL;
  class->mark = NULL;
  class->is_marked = NULL;
  class->set_re_lexed = NULL;
  class->was_re_lexed = NULL;
  class->insert_text = NULL;
  class->delete_text = NULL;
}

static void
gpf_token_init (GpfToken *token)
{
}

char *
gpf_token_lexeme (GpfToken *token, GpfVersion version)
{
  GpfNode *node = GPF_NODE (token);
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->lexeme != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->lexeme (token, version);
}

int
gpf_token_get_state (GpfToken *token, GpfVersion version)
{
  GpfNode *node = GPF_NODE (token);
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->get_state != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->get_state (token, version);
}

void
gpf_token_set_state (GpfToken *token, int state)
{
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->set_state != NULL);

  class->set_state (token, state);
}

int
gpf_token_get_lookback (GpfToken *token, GpfVersion version)
{
  GpfNode *node = GPF_NODE (token);
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->get_lookback != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->get_lookback (token, version);
}

void
gpf_token_set_lookback  (GpfToken *token, int lookback)
{
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->set_lookback != NULL);

  class->set_lookback (token, lookback);
}

int
gpf_token_get_lookahead (GpfToken *token, GpfVersion version)
{
  GpfNode *node = GPF_NODE (token);
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->get_lookahead != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->get_lookahead (token, version);
}

void
gpf_token_set_lookahead (GpfToken *token, int lookahead)
{
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->set_lookahead != NULL);

  class->set_lookahead (token, lookahead);
}

void
gpf_token_mark (GpfToken *token)
{
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->mark != NULL);

  class->mark (token);
}

gboolean
gpf_token_is_marked (GpfToken *token)
{
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->is_marked != NULL);

  return class->is_marked (token);
}

void
gpf_token_set_re_lexed (GpfToken *token)
{
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->set_re_lexed != NULL);

  class->set_re_lexed (token);
}

gboolean
gpf_token_was_re_lexed (GpfToken *token)
{
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->was_re_lexed != NULL);

  return class->was_re_lexed (token);
}

GpfToken *
gpf_token_previous (GpfToken *token, GpfVersion version)
{
  GpfNode *node, *child = GPF_NODE (token);
  int i;

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  for (node = gpf_node_get_parent (GPF_NODE(token), version);
       node != NULL;
       child = node, node = gpf_node_get_parent (node, version)) {
    for (i = gpf_node_has_child (node, child, version) - 1;
         i >= 0; i--) {
      if ((token = _gpf_token_dfs (gpf_node_get_child (node, i, version), version)))
        return token;
    }
  }

  return NULL;
}

GpfToken *
gpf_token_next (GpfToken *token, GpfVersion version)
{
  GpfNode *node, *child = GPF_NODE (token);
  int i;

  if (version == GPF_REFERENCE_VERSION)
    version = child->document->reference_version;

  for (node = gpf_node_get_parent (GPF_NODE(token), version);
       node != NULL;
       child = node, node = gpf_node_get_parent (node, version)) {
    for (i = gpf_node_has_child (node, child, version) + 1;
         i < gpf_node_arity (node, version); i++) {
      if ((token = _gpf_token_dfs (gpf_node_get_child (node, i, version), version)))
        return token;
    }
  }

  return NULL;
}

void
gpf_token_insert_text (GpfToken *token, int offset,
                       const char *text, int length)
{
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->insert_text != NULL);

  class->insert_text (token, offset, text, length);
}

void
gpf_token_delete_text (GpfToken *token, int start, int end)
{
  GpfTokenClass *class = GPF_TOKEN_GET_CLASS (token);
  g_assert (class->delete_text != NULL);

  class->delete_text (token, start, end);
}
