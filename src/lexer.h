/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __GPF_LEXER_H__
#define __GPF_LEXER_H__

#include "gpfdocument.h"
#include "gpftoken.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* special tokens produced by the batch lexer */
#define GPF_COMMENT          -2  /* comment token */
#define GPF_WHITE_SPACE      -3  /* white space token */
#define GPF_TOKEN_SEQUENCE   -4  /* token sequence created using
                                    arbitrary code */
#define GPF_I_TOKEN_SEQUENCE -5  /* indestructable token sequence,
                                    does not guarantee to map to
                                    character stream, therefore it
                                    can't be modified except in
                                    whole. */

int  gpf_next_char ();
void gpf_mark_tokens (GpfDocument *document);
void gpf_lex (GpfDocument *document);
GpfToken *gpf_relex (GpfToken *token, GpfDocument *document);
GpfToken *gpf_next_new_token ();
void gpf_update_lookbacks (GpfDocument *document);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* __GPF_LEXER_H__ */
