/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __COMMON_H__
#define __COMMON_H__

#include "gpfnode.h"
#include "gpftoken.h"
#include "version.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

GpfToken *_gpf_token_dfs (GpfNode *root, GpfVersion version);
GpfToken *_gpf_first_token_after(GpfNode *node, GpfVersion version);
GpfToken *_gpf_first_token (GpfNode *node, GpfVersion version);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __COMMON_H__ */
