/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/* Test program should display the following things:
  
   global versioning tree
   abstract syntax tree
   program text
*/

#include <gtk/gtk.h>
#include <stdio.h>
#include <string.h>

#include "gpfdocument.h"
#include "gpfnode.h"
#include "gpftoken.h"

GpfDocument *document;

GtkWidget *text_view;
GtkWidget *gvt_view;
GtkWidget *ast_view;

/* gtk signal handlers */
static void
insert_text (GtkEditable *editable, const gchar *new_text,
             gint new_text_length, gint *position)
{
  gpf_document_begin_edit (document);
  gpf_document_insert_text (document, *position, new_text, new_text_length);
  gpf_document_end_edit (document);
  gpf_document_update (document);
}

static void
delete_text (GtkEditable *editable, gint start_pos, gint end_pos)
{
  gpf_document_begin_edit (document);
  gpf_document_delete_text (document, start_pos, end_pos);
  gpf_document_end_edit (document);
  gpf_document_update (document);
}

static GtkWidget *old_selection;

void
version_node_selected (GtkTree *tree, gpointer user_data)
{
  static GtkWidget *old_child;
  GtkWidget *child;

  if (!GTK_TREE_SELECTION (tree))
    return;

  child = GTK_TREE_SELECTION (tree)->data;
  if (child == old_child)
    gtk_signal_emit_stop_by_name (GTK_OBJECT(tree), "selection_changed");
  gtk_tree_unselect_child (tree, child);
  gtk_list_item_select (GTK_LIST_ITEM (old_selection));
  old_child = child;
}

static gboolean version_selected_enable = TRUE; 

void
version_selected (GtkList *list, GtkWidget *widget, gpointer user_data)
{
  int *version = gtk_object_get_data (GTK_OBJECT (widget), "version");

  /* to prevent unwanted calls */
  if (version_selected_enable) {
    if (old_selection && old_selection != widget)
      gtk_list_item_deselect (GTK_LIST_ITEM (old_selection));

    gpf_document_set_version(document, *version);
    old_selection = widget;
  }
}

/* view initalizers */
static void
init_text ()
{
  GpfToken *token = document->bos;

  gtk_text_set_point (GTK_TEXT (text_view), 0);
  gtk_text_forward_delete (GTK_TEXT (text_view), -1);

  for (token = document->bos; token != document->eos; token = gpf_token_next (token, GPF_CURRENT_VERSION)) {
    char *lexeme = gpf_token_lexeme (token, GPF_CURRENT_VERSION);
    if (lexeme)
      gtk_text_insert (GTK_TEXT (text_view), NULL, NULL, NULL, lexeme, -1);
  }
}

static void
append_gvt_node (GpfGvtNode *node, GtkWidget *parent)
{
  GpfVersionInfo *info;
  GtkWidget *tree_item;
  GtkWidget *list_item;
  GtkWidget *list;
  GtkWidget *frame;
  GList *items = NULL;
  int *version, v;

  tree_item = gtk_tree_item_new ();
  gtk_widget_show (tree_item);
  frame = gtk_frame_new (NULL);
    gtk_container_add (GTK_CONTAINER (tree_item), frame);
  gtk_widget_show (frame);
  gtk_object_set_data (GTK_OBJECT (tree_item), "version_node", node);
  list = gtk_list_new ();
  gtk_widget_show (list);
  gtk_signal_connect_after (GTK_OBJECT (list), "select_child",
		      GTK_SIGNAL_FUNC (version_selected),
		      NULL);
  gtk_list_set_selection_mode (GTK_LIST(list), GTK_SELECTION_SINGLE);
  for (v = node->start_version; v <= node->end_version; v++) {
    char num [64];

    sprintf (num, "%d", v);
    list_item = gtk_list_item_new_with_label (num);
    gtk_widget_show (list_item);
    version = g_new (int, 1);
    *version = v;

    if (v == document->gvt->global_version) {
      gtk_list_item_select (GTK_LIST_ITEM (list_item));
      old_selection = list_item;
    } else
      gtk_list_item_deselect (GTK_LIST_ITEM (list_item));

    gtk_object_set_data (GTK_OBJECT (list_item), "version", version);
    info = gpf_document_get_version_info (document, v);
    g_datalist_set_data (&info->data, "gvt_view_item", list_item);
    items = g_list_append (items, list_item);
  }

  gtk_list_append_items (GTK_LIST(list), items);
  gtk_container_add (GTK_CONTAINER (frame), list);
  g_datalist_set_data (&node->data, "gvt_view_node", tree_item);
  gtk_tree_append (GTK_TREE (parent), tree_item);

  /* process the child gvt nodes */
  if (node->children) {
    parent = gtk_tree_new ();
    gtk_widget_show (parent);
    gtk_tree_item_set_subtree (GTK_TREE_ITEM (tree_item), parent);
    gtk_tree_item_expand (GTK_TREE_ITEM(tree_item));

    for (node = node->children; node != NULL; node = node->next) {
      append_gvt_node (node, parent);
    }
  }
}

static void
init_gvt ()
{
  GpfVersionInfo *info;
  GtkWidget *tree_item;
  GtkWidget *list_item;
  GtkWidget *frame;
  GtkWidget *list;
  GpfGvtNode *node;
  GList *items = NULL;
  int *version;

  version_selected_enable = FALSE;
  /* create the intial item */
  list_item = gtk_list_item_new_with_label ("0 - Initial version");
  if (document->gvt->global_version == 0) {
    gtk_list_item_select (GTK_LIST_ITEM (list_item));
    old_selection = list_item;
  } else
    gtk_list_item_deselect (GTK_LIST_ITEM (list_item));
  
  version = g_new (int, 1);
  *version = 0;
  gtk_object_set_data (GTK_OBJECT (list_item), "version", version);
  info = gpf_document_get_version_info (document, 0);
  g_datalist_set_data (&info->data, "gvt_view_item", list_item);

  tree_item = gtk_tree_item_new ();
  node = gpf_gvt_get_node (document->gvt, 0);
  gtk_object_set_data (GTK_OBJECT (tree_item), "version_node", node);
  list = gtk_list_new ();
  gtk_signal_connect_after (GTK_OBJECT (list), "select_child",
		      GTK_SIGNAL_FUNC (version_selected),
		      NULL);
  gtk_list_set_selection_mode (GTK_LIST(list), GTK_SELECTION_SINGLE);
  items = g_list_append (items, list_item);
  gtk_list_append_items (GTK_LIST(list), items);
  frame = gtk_frame_new (NULL);
  gtk_widget_show (frame);
  gtk_container_add (GTK_CONTAINER (frame), list);
  gtk_container_add (GTK_CONTAINER (tree_item), frame);
  g_datalist_set_data (&node->data, "gvt_view_node", tree_item);

  gtk_tree_append (GTK_TREE (gvt_view), tree_item);

  gtk_widget_show (tree_item);
  gtk_widget_show (list);
  gtk_widget_show (list_item);

  /* process the sentinel nodes children */
  if (node->children) {
    GtkWidget *parent = gtk_tree_new ();
    gtk_widget_show (parent);
    gtk_tree_item_set_subtree (GTK_TREE_ITEM (tree_item), parent);
    gtk_tree_item_expand (GTK_TREE_ITEM(tree_item));
    for (node = node->children; node != NULL; node = node->next) {
      append_gvt_node (node, parent);
    }
  }

  version_selected_enable = TRUE;
}

extern const char *symbol_name (int symbol);
extern const char *token_name (int token_no);

static void
insert_ast_node (GtkWidget *parent, GpfNode *node, int p)
{
  GtkWidget *dnode;
  int arity;

  if (!node)
    return;
  
  dnode = gtk_object_get_data (GTK_OBJECT (node), "display_node");
  if (!dnode) {
    const char *name;

    if (GPF_IS_TOKEN (node))
      name = token_name (gpf_node_get_symbol(node, GPF_CURRENT_VERSION));
    else
      name = symbol_name (gpf_node_get_symbol(node, GPF_CURRENT_VERSION));

    dnode = gtk_tree_item_new_with_label (name);
    gtk_object_set_data (GTK_OBJECT (node), "display_node", dnode);
  }
  gtk_widget_show (dnode);

  gtk_tree_insert (GTK_TREE (parent), dnode, p);

  if ((arity = gpf_node_arity (node, GPF_CURRENT_VERSION))) {
    int i;
    GtkWidget *tree = GTK_TREE_ITEM_SUBTREE(dnode);

    if (tree)
      return;

    tree  = gtk_tree_new ();
    gtk_widget_show (tree);
    gtk_tree_item_set_subtree (GTK_TREE_ITEM (dnode), tree);  
    for (i = 0; i < arity; i++) {
      insert_ast_node (tree, gpf_node_get_child(node, i,
                                                GPF_CURRENT_VERSION), i);
    } 
  }  
}

static void
init_ast ()
{
  GtkWidget *dnode;
  GtkWidget *tree;
  int arity;

  /* initialize the sentinel nodes */
  dnode = gtk_tree_item_new_with_label ("ultra_root");
  gtk_object_set_data (GTK_OBJECT(document->ultra_root), "display_node", dnode);
  gtk_widget_show (dnode);
  gtk_tree_append (GTK_TREE(ast_view), dnode);
  tree = gtk_tree_new ();
  gtk_widget_show (tree);
  gtk_tree_item_set_subtree (GTK_TREE_ITEM (dnode), tree);
  dnode = gtk_tree_item_new_with_label ("BOS");
  gtk_object_set_data (GTK_OBJECT(document->bos), "display_node", dnode);
  gtk_widget_show (dnode);
  gtk_tree_append (GTK_TREE(tree), dnode);
  dnode = gtk_tree_item_new_with_label ("EOS");
  gtk_object_set_data (GTK_OBJECT(document->eos), "display_node", dnode);
  gtk_widget_show (dnode);
  gtk_tree_append (GTK_TREE(tree), dnode);
  
  /* initialize the tree  */
  if ((arity = gpf_node_arity (document->ultra_root, GPF_CURRENT_VERSION)) > 2) {
    int i;
    for (i = 1; i < arity - 1; i++) {
      insert_ast_node (tree, 
        gpf_node_get_child (document->ultra_root, i,
                            GPF_CURRENT_VERSION), i);
    }
  } 
}

/* view updaters */

static void
update_text (GpfNode *node, int position,
             GpfVersion prev_version, GpfVersion cur_version)
{
  int i;
  gboolean child_changes;

  if (GPF_IS_TOKEN (node)) {
    if (gpf_node_has_changes (node, GPF_TEXT_CHANGES, cur_version)) {
      int length = gpf_node_get_length (node, prev_version);
      char *lexeme = gpf_token_lexeme (GPF_TOKEN(node), cur_version);
      gtk_text_set_point (GTK_TEXT (text_view), position);
      gtk_text_forward_delete (GTK_TEXT (text_view), length);
      if (lexeme)
        gtk_text_insert (GTK_TEXT (text_view), NULL, NULL, NULL, lexeme, -1);
    }
    return;
  }

  child_changes = gpf_node_has_changes (node, GPF_CHILD_CHANGES, cur_version);
  for (i = 0; i < gpf_node_arity (node, cur_version); i++) {
    GpfNode *child = gpf_node_get_child (node, i, cur_version);
    if (child_changes) {
      GpfNode *old_child = gpf_node_get_child (node, i, prev_version);

      if (old_child && old_child != child) {
        gtk_text_set_point (GTK_TEXT (text_view), position);
        gtk_text_forward_delete (GTK_TEXT (text_view), gpf_node_get_length (old_child, prev_version));
      }
    }
    if (child) {
      if (gpf_node_has_changes (child, GPF_NESTED_CHANGES | GPF_LOCAL_CHANGES, cur_version)) {
        update_text (child, position, prev_version, cur_version);
      }
      position += gpf_node_get_length (child, cur_version);
    }
  }
}

static void
update_gvt (GpfDocument *document, GpfVersion prev_version,
            GpfVersion cur_version)
{
  GpfVersionInfo *info;
  GpfGvtNode *parent_node, *node;
  int *version;

  /* prevent version_selected from being called */
  version_selected_enable = FALSE;

  info = gpf_document_get_version_info (document, cur_version);

  if (g_datalist_get_data(&info->data, "gvt_view_item") == NULL) {
    char buffer[128];
    GtkWidget *parent_item;
    GtkWidget *subtree;
    GtkWidget *list;
    GtkWidget *frame;
    GList *items = NULL;
    GtkWidget *list_item;
    GtkWidget *tree_item;

    sprintf (buffer, "%d", cur_version);
    list_item = gtk_list_item_new_with_label (buffer);
    gtk_widget_show (list_item);
    version = g_new (int, 1);
    *version = cur_version;
    gtk_object_set_data (GTK_OBJECT (list_item), "version", version);
    g_datalist_set_data (&info->data, "gvt_view_item", list_item);
    
    parent_node = gpf_gvt_get_node (document->gvt, prev_version);
    node = gpf_gvt_get_node (document->gvt, cur_version);
    info = gpf_document_get_version_info (document, prev_version);
    list = gtk_container_children (GTK_CONTAINER(gtk_container_children (GTK_CONTAINER(g_datalist_get_data(&parent_node->data, "gvt_view_node")))->data))->data;
    gtk_list_unselect_all (GTK_LIST(list));
    
    /* check for new node */ 
    if (parent_node != node) {
      parent_item = g_datalist_get_data(&node->parent->data, "gvt_view_node");
      /* check for a split */
      if ((parent_node->end_version - parent_node->start_version + 1) 
           < g_list_length(gtk_container_children (GTK_CONTAINER(list)))) {
        GpfGvtNode *split_node = node->next;
        GtkWidget *parent_list = gtk_container_children(GTK_CONTAINER(gtk_container_children(GTK_CONTAINER(parent_item))->data))->data;
        GList *child_list = gtk_container_children(GTK_CONTAINER(parent_list));
        GList *temp_list = NULL;



        /* create split node */
        tree_item = gtk_tree_item_new ();
        gtk_widget_show (tree_item);
        gtk_object_set_data (GTK_OBJECT (tree_item), "version_node", split_node);
        frame = gtk_frame_new (NULL);
        gtk_widget_show (frame);
        gtk_container_add (GTK_CONTAINER (tree_item), frame);
        list = gtk_list_new ();
        gtk_widget_show (list);
        gtk_signal_connect_after (GTK_OBJECT (list), "select_child",
		      GTK_SIGNAL_FUNC (version_selected),
		      NULL);
        gtk_list_set_selection_mode (GTK_LIST(list), GTK_SELECTION_BROWSE);
        
        gtk_container_add (GTK_CONTAINER (frame), list);
        g_datalist_set_data (&split_node->data, "gvt_view_node", tree_item);

        /* get split items from parent */
        child_list = g_list_nth (child_list, node->parent->end_version - node->parent->start_version + 1);
        for (; child_list != NULL; child_list = child_list->next)
          temp_list = g_list_prepend (temp_list, child_list->data);
        temp_list = g_list_reverse (temp_list);
        gtk_list_remove_items_no_unref (GTK_LIST (parent_list), temp_list);

        /* add items to split node */
        gtk_list_append_items (GTK_LIST(list), temp_list);
        gtk_list_unselect_all (GTK_LIST(list));
        /* fix the subtrees */
        if ((subtree = GTK_TREE_ITEM_SUBTREE(parent_item))) {
          gtk_tree_item_remove_subtree (GTK_TREE_ITEM(parent_item));
          gtk_tree_item_set_subtree (GTK_TREE_ITEM(tree_item), subtree);
        }
        subtree = gtk_tree_new();
        gtk_widget_show (subtree);
        gtk_tree_append (GTK_TREE (subtree), tree_item);
        gtk_tree_item_set_subtree(GTK_TREE_ITEM(parent_item), subtree);
      }

      tree_item = gtk_tree_item_new ();
      gtk_widget_show (tree_item);
      /*gtk_tree_item_select (GTK_TREE_ITEM (tree_item));*/
      gtk_object_set_data (GTK_OBJECT (tree_item), "version_node", node);
      frame = gtk_frame_new (NULL);
      gtk_widget_show (frame);
      gtk_container_add (GTK_CONTAINER (tree_item), frame);
      list = gtk_list_new ();
      gtk_widget_show (list);
      gtk_signal_connect_after (GTK_OBJECT (list), "select_child",
		                GTK_SIGNAL_FUNC (version_selected), NULL);
      gtk_list_set_selection_mode (GTK_LIST(list), GTK_SELECTION_BROWSE);
      items = g_list_append (items, list_item);
      gtk_list_append_items (GTK_LIST(list), items);
      gtk_container_add (GTK_CONTAINER (frame), list);
      g_datalist_set_data (&node->data, "gvt_view_node", tree_item);

      subtree = GTK_TREE_ITEM_SUBTREE(parent_item);
      if (subtree == NULL) {
        subtree = gtk_tree_new();
        gtk_widget_show (subtree);
        gtk_tree_item_set_subtree(GTK_TREE_ITEM(parent_item), subtree);
      }
      gtk_tree_item_expand (GTK_TREE_ITEM(parent_item));
      gtk_tree_append (GTK_TREE (subtree), tree_item);
    } else {
      list = gtk_container_children(GTK_CONTAINER(gtk_container_children(GTK_CONTAINER(g_datalist_get_data(&node->data, "gvt_view_node")))->data))->data;
      items = g_list_append (items, list_item);
      gtk_list_append_items (GTK_LIST(list), items);
      gtk_list_item_select (GTK_LIST_ITEM (list_item));
    }
  }
 
  /* enable version_selected */
  version_selected_enable = TRUE;
}

static void
update_removed_nodes (GpfNode *node, GpfVersion prev_version,
                      GpfVersion cur_version)
{
  GtkWidget *tree;
  GtkWidget *dnode;
  int i, length = gpf_node_arity (node, cur_version);
  GpfNode *child;
  gboolean child_changes = FALSE;

  if (gpf_node_has_changes (node, GPF_CHILD_CHANGES, cur_version))
    child_changes = TRUE;
   
  dnode = gtk_object_get_data (GTK_OBJECT(node), "display_node");
  tree = GTK_TREE_ITEM_SUBTREE (dnode); 

  for (i = 0; i < length; i++) {
    child = gpf_node_get_child (node, i, cur_version);
    
    if (child_changes) {
      GpfNode *old_child = gpf_node_get_child (node, i, prev_version);

      if (old_child && child != old_child) {
        dnode = gtk_object_get_data (GTK_OBJECT(old_child), "display_node");
        if (dnode)
          gtk_tree_remove_item (GTK_TREE(tree), dnode);
      }  
    }
    
    if (child && gpf_node_has_changes (node, GPF_NESTED_CHANGES | GPF_LOCAL_CHANGES, cur_version))
      update_removed_nodes (child, prev_version, cur_version); 
  }
}

/* note: this view will serve as a parse tree for now instead of an
   abstract syntax tree */
static void
update_changed_nodes (GpfNode *node, GpfVersion prev_version,
                      GpfVersion cur_version)
{
  GtkWidget *tree;

  if (gpf_node_has_changes (node, GPF_CHILD_CHANGES, cur_version)) {
     int i, length = gpf_node_arity (node, prev_version);
    GpfNode *child; GpfNode *old_child;

    for (i = 0; i < length; i++) {
      child = gpf_node_get_child (node, i, cur_version);
      old_child = gpf_node_get_child (node, i, prev_version);

      if (child != old_child) {
        insert_ast_node (tree, child, i);
      }  
    }
  }
}

static void
update_ast (GpfDocument *document, GpfVersion prev_version,
            GpfVersion cur_version)
{
  update_removed_nodes (document->ultra_root, prev_version, cur_version);
  update_changed_nodes (document->ultra_root, prev_version, cur_version);
}

static void
document_changed (GpfDocument *document, GpfVersion prev_version,
                  GpfVersion cur_version)
{
  GpfVersionInfo *info;

  /* update views */
  info = gpf_document_get_version_info (document, cur_version);
  if (g_datalist_get_data(&info->data, "gvt_view_item")) 
    update_text (document->ultra_root, 0, prev_version, cur_version);
  update_gvt (document, prev_version, cur_version);
  update_ast (document, prev_version, cur_version);
}

static void
quit (void)
{
  gtk_exit (0);
}

static GtkWidget*
create_editor_window (void)
{
  GtkWidget *window;
  GtkWidget *scrolledwindow1;
  
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize (window, 350, 450);
  gtk_window_set_title (GTK_WINDOW (window), "Editor");
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (quit),
		      NULL);

  scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scrolledwindow1),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_AUTOMATIC);
  gtk_widget_show (scrolledwindow1);
  gtk_container_add (GTK_CONTAINER (window), scrolledwindow1);

  text_view = gtk_text_new (NULL, NULL);
  init_text ();
  gtk_text_set_editable (GTK_TEXT (text_view), TRUE);
  gtk_signal_connect (GTK_OBJECT (text_view), "insert_text",
		      GTK_SIGNAL_FUNC (insert_text),
		      NULL);
  gtk_signal_connect (GTK_OBJECT (text_view), "delete_text",
		      GTK_SIGNAL_FUNC (delete_text),
		      NULL);
  gtk_widget_show (text_view);
  gtk_container_add (GTK_CONTAINER (scrolledwindow1), text_view);

  return window;
}

static void
book_switch_page (GtkNotebook     *nbook,
		  GtkNotebookPage *page)
{
  GtkWidget *window = gtk_object_get_data (GTK_OBJECT(nbook), "window");

  gtk_window_set_title (GTK_WINDOW (window), gtk_object_get_data (GTK_OBJECT(page->child), "title"));
}

static GtkWidget*
create_gvt_ast_window (void)
{
  GtkWidget *window;
  GtkWidget *label;
  GtkWidget *notebook;
  GtkWidget *scrolledwindow;
  GtkWidget *viewport;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize (window, 200, 450);
  gtk_window_set_title (GTK_WINDOW (window), "Global Versioning Tree");

  notebook = gtk_notebook_new ();
  gtk_notebook_set_tab_pos (GTK_NOTEBOOK(notebook), GTK_POS_BOTTOM);
  gtk_signal_connect (GTK_OBJECT (notebook), "switch_page",
		      GTK_SIGNAL_FUNC (book_switch_page), NULL);
  gtk_object_set_data (GTK_OBJECT (notebook), "window", window);
  gtk_widget_show (notebook);
  gtk_container_add (GTK_CONTAINER (window), notebook);

  scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scrolledwindow),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_AUTOMATIC);
  gtk_object_set_data (GTK_OBJECT (scrolledwindow), "title", strdup ("Global Versioning Tree"));
  gtk_widget_show (scrolledwindow);
  label = gtk_label_new ("gvt");
  gtk_notebook_append_page (GTK_NOTEBOOK(notebook), scrolledwindow, label);

  viewport = gtk_viewport_new (NULL, NULL);
  gtk_widget_show (viewport);
  gtk_container_add (GTK_CONTAINER (scrolledwindow), viewport);

  gvt_view = gtk_tree_new ();
  gtk_tree_set_view_mode (GTK_TREE (gvt_view), GTK_TREE_VIEW_ITEM);
  gtk_tree_set_selection_mode (GTK_TREE (gvt_view), GTK_SELECTION_SINGLE);
  gtk_signal_connect (GTK_OBJECT (gvt_view), "selection_changed",
		      GTK_SIGNAL_FUNC (version_node_selected),
		      NULL);
  init_gvt ();


  gtk_widget_show (gvt_view);

  gtk_container_add (GTK_CONTAINER (viewport), gvt_view);

  scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scrolledwindow),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_AUTOMATIC);
  gtk_object_set_data (GTK_OBJECT (scrolledwindow), "title", strdup ("Abstract Syntax Tree"));
  gtk_widget_show (scrolledwindow);
  label = gtk_label_new ("ast");
  gtk_notebook_append_page (GTK_NOTEBOOK(notebook), scrolledwindow, label);

  viewport = gtk_viewport_new (NULL, NULL);
  gtk_widget_show (viewport);
  gtk_container_add (GTK_CONTAINER (scrolledwindow), viewport);

  ast_view = gtk_tree_new ();
  gtk_widget_show (ast_view);

  init_ast ();

  gtk_widget_show (ast_view);
  gtk_container_add (GTK_CONTAINER (viewport), ast_view);

  return window;
}

int main(int argc, char *argv[])
{
  GtkWidget *text_window, *gvt_ast_window;

  gtk_init (&argc, &argv);

  document = gpf_document_open ("testfile.c");
  gpf_document_update (document);
  gtk_signal_connect (GTK_OBJECT (document), "changed",
		      GTK_SIGNAL_FUNC (document_changed),
		      NULL);

  text_window = create_editor_window();
  gvt_ast_window = create_gvt_ast_window();
  
  gtk_widget_show (text_window);
  gtk_widget_show (gvt_ast_window);

  gtk_main ();

  return 0;
}
