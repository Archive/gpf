#include "common.h"

GpfToken *
_gpf_token_dfs (GpfNode *root, GpfVersion version)
{
  GpfToken *token;
  int i;

  if (root == NULL)
    return NULL;
  if (GPF_IS_TOKEN (root))
    return GPF_TOKEN (root);
  for (i = 0; i < gpf_node_arity (root, version); i++) {
    if ((token = _gpf_token_dfs (gpf_node_get_child (root, i, version), version)))
      return token;
  }
  return NULL;
}

GpfToken *
_gpf_first_token_after(GpfNode *node, GpfVersion version)
{
  GpfNode *child = node;
  GpfToken *token;

  for (node = gpf_node_get_parent (node, version);
       node != NULL;
       child = node, node = gpf_node_get_parent (node, version)) {
    int i;
    for (i = gpf_node_has_child (node, child, version) + 1;
         i < gpf_node_arity (node, version); i++) {
      if ((token = _gpf_token_dfs (gpf_node_get_child (node, i, version), version)))
        return token;
    }
  }

  return NULL;
}

GpfToken *
_gpf_first_token (GpfNode *node, GpfVersion version)
{
  GpfToken *token;

  if ((token = _gpf_token_dfs (node, version)))
    return token;

  return _gpf_first_token_after (node, version);
}
