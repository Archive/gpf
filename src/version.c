/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
  
#include <glib.h>
#include "version.h"

#define gvt_version_to_gvt_node(gvt, version) \
   g_ptr_array_index (gvt->version_node_map, version)

/* -- gpf global versioning tree -- */
GpfGvt *
gpf_gvt_new ()
{
  GpfGvt *gvt;

  gvt = g_new (GpfGvt, 1);
  gvt->root = NULL;
  gvt->version_node_map = g_ptr_array_new ();
  gvt->preorder_map = g_array_new (FALSE, FALSE, sizeof (GpfGvtNode *));
  gvt->postorder_map = g_array_new (FALSE, FALSE, sizeof (GpfGvtNode *));
  gvt->global_version = -1;

  return gvt;
}

void
gpf_gvt_free (GpfGvt *gvt)
{
  int i;

  for (i = 0; i < gvt->version_node_map->len; i++)
    g_free (g_ptr_array_index (gvt->version_node_map, i));

  g_ptr_array_free (gvt->version_node_map, TRUE);
  g_array_free (gvt->preorder_map, TRUE);
  g_array_free (gvt->postorder_map, TRUE);

  g_free (gvt);
}

int
gpf_gvt_new_version (GpfGvt *gvt)
{
  GpfGvtNode *cur;

  if (gvt->global_version == -1) {
    gvt->root = g_new (GpfGvtNode, 1);
    gvt->root->preorder_index = 0;
    gvt->root->postorder_index = 0;
    gvt->root->start_version = 0;
    gvt->root->end_version = 0;
    gvt->root->parent = NULL;
    gvt->root->children = NULL;
    gvt->root->prev = NULL;
    gvt->root->next = NULL;
    g_datalist_init (&gvt->root->data);
    g_ptr_array_add (gvt->version_node_map, gvt->root);
    g_array_append_val (gvt->preorder_map, gvt->root);
    g_array_append_val (gvt->postorder_map, gvt->root);

    gvt->global_version = 0;

    return gvt->global_version;
  }

  cur = g_ptr_array_index (gvt->version_node_map, gvt->global_version);

  if (gvt->global_version != 0 && gvt->global_version == gvt->version_node_map->len -1) {
    /* Colapse linear modifications into a single gvt node. */
    g_ptr_array_add (gvt->version_node_map, cur);
    cur->end_version = gvt->global_version + 1;
  } else {
    GpfGvtNode *node = g_new (GpfGvtNode, 1);
    int i;

    g_ptr_array_add (gvt->version_node_map, node);

    /* check for splits */
    if (gvt->global_version < cur->end_version) {
      GpfGvtNode *split_node = g_new (GpfGvtNode, 1);

      g_datalist_init (&split_node->data);
      split_node->start_version = gvt->global_version + 1;
      split_node->end_version = cur->end_version;
      split_node->parent = cur;
      split_node->children = cur->children;
      cur->end_version = gvt->global_version;
      cur->children = split_node;

      /* Insert the split node into the node maps. */
      g_array_insert_val (gvt->preorder_map, cur->preorder_index+1, split_node);
      g_array_insert_val (gvt->postorder_map, cur->postorder_index, split_node);

      /* Update the version to node map. */
      for (i = gvt->global_version+1; i <= split_node->end_version; i++)
        g_ptr_array_index (gvt->version_node_map, i) = split_node;
    }

    g_datalist_init (&node->data);
    node->preorder_index = cur->preorder_index+1;
    if (cur->prev)
      node->postorder_index = cur->prev->postorder_index+1;
    else
      node->postorder_index = 0;
    node->start_version = gvt->version_node_map->len - 1;
    node->end_version = node->start_version;
    node->parent = cur;
    node->children = NULL;
    node->prev = NULL;
    node->next = cur->children;

    if (cur->children)
      cur->children->prev = node;
    cur->children = node;

    /* Insert the new node into the node maps. */
    g_array_insert_val (gvt->preorder_map, node->preorder_index, node);
    g_array_insert_val (gvt->postorder_map, node->postorder_index, node);

    /* Update the node maps */
    for (i = node->preorder_index+1; i < gvt->preorder_map->len; i++)
      g_array_index (gvt->preorder_map, GpfGvtNode *, i)->preorder_index = i;

    for (i = node->postorder_index+1; i < gvt->postorder_map->len; i++)
      g_array_index (gvt->postorder_map, GpfGvtNode *, i)->postorder_index = i;
  }

  gvt->global_version = gvt->version_node_map->len - 1;

  return gvt->global_version;
}

int
gpf_gvt_last_version (GpfGvt *gvt)
{
  return gvt->version_node_map->len - 1;
}

int
gpf_gvt_preorder_compare (GpfGvtNode *a, GpfGvtNode *b)
{
  return a->preorder_index - b->preorder_index;
}

/* -- gpf versioned boolean -- */
GpfVBoolean *
gpf_vboolean_new ()
{
  GpfVBoolean *vboolean = g_new (GpfVBoolean, 1);

  vboolean->log_index = -1;
  vboolean->log = NULL;
  vboolean->vlog = NULL;
  vboolean->len = 0;

  return vboolean;
}

void
gpf_vboolean_free (GpfVBoolean *vboolean)
{
  g_free (vboolean->log);
  g_free (vboolean->vlog);
  g_free (vboolean);
}

gboolean
gpf_vboolean_get(GpfVBoolean *vboolean, GpfGvt *gvt, GpfVersion version)
{
  int index;

  if (version == GPF_CURRENT_VERSION)
    return vboolean->log[vboolean->log_index / 8] & (1 << (vboolean->log_index % 8));

  if (version == GPF_PREVIOUS_VERSION) {
    if (gpf_vboolean_changed(vboolean))
      return vboolean->log[(vboolean->log_index-1) / 8] & (1 << ((vboolean->log_index-1) % 8));
                                        \
    return vboolean->log[vboolean->log_index / 8] & (1 << (vboolean->log_index % 8));
  }

  index = gpf_vobject_project ((GpfVObject *)vboolean, (gvt), (version));
  return vboolean->log[index / 8] & (1 << (index % 8));
}

void
gpf_vboolean_set (GpfVBoolean *vboolean, GpfGvt *gvt, gboolean value)
{
  if (!gpf_vboolean_changed (vboolean)) {
    int i;
    unsigned char mask;

    vboolean->len++;
    vboolean->log_index++;
    if ((vboolean->log == NULL) || ((vboolean->len % 8) == 0)) {
      vboolean->log = g_realloc (vboolean->log, vboolean->len / 8 + 1);
      vboolean->log [vboolean->len / 8] = 0;
    }
    vboolean->vlog = g_realloc (vboolean->vlog, vboolean->len * sizeof (GpfVersion));
    memmove (vboolean->vlog + vboolean->log_index + 1,
             vboolean->vlog + vboolean->log_index,
             (vboolean->len - vboolean->log_index - 1) * sizeof (GpfVersion));
    vboolean->vlog [vboolean->log_index] = gpf_gvt_last_version (gvt) + 1;
    /* shift trailing bits to the left */
    for (i = vboolean->len / 8; i > vboolean->log_index / 8; i--)
      vboolean->log[i] = (vboolean->log[i] << 1)
                          | ((vboolean->log[i-1] & 0x80) >> 7);
    mask = (0xFF << ((vboolean->log_index % 8)+1));
    vboolean->log[i] = ((vboolean->log[i] & mask) << 1) | (vboolean->log[i] & ~mask); 
    /* set changed bit */
    vboolean->log[vboolean->len / 8] |= 0x80;
  }

  /* set log value */
  vboolean->log[vboolean->log_index / 8] |= ((value != 0) << (vboolean->log_index % 8));
}

void
gpf_vboolean_commit (GpfVBoolean *vboolean)
{
  if (gpf_vboolean_changed (vboolean)) {
    /* unset the changed bit */
    vboolean->log[vboolean->len / 8] ^= 0x80;
  }
}

void
gpf_vboolean_discard_changes (GpfVBoolean *vboolean)
{
  if (!gpf_vboolean_changed (vboolean))
    return;
  vboolean->log[vboolean->len / 8] ^= 0x80;
  vboolean->len--;
  vboolean->log[vboolean->len / 8] ^= 0x80;
}

/* -- gpf versioned object -- */
GpfVObject *
gpf_vobject_new ()
{
  GpfVObject *vobject = g_new (GpfVObject, 1);

  vobject->log_index = -1;
  vobject->log = NULL;
  vobject->vlog = NULL;
  vobject->len = 0;
  vobject->changed = FALSE;

  return vobject;
}

void
gpf_vobject_free (GpfVObject *vobject)
{
  g_free (vobject->log);
  g_free (vobject->vlog);
  g_free (vobject);
}

/* Map a global version to a local version (log index). */
int
gpf_vobject_project (GpfVObject *vobject, GpfGvt *gvt, GpfVersion version)
{
  int l = 0, r = vobject->len - 1;
  int x, result;
  GpfGvtNode *gd1, *gd2;

  gd1 = gvt_version_to_gvt_node(gvt, version);
  do {
    x = (l+r) / 2;
    gd2 = gvt_version_to_gvt_node(gvt, vobject->vlog[x]);
    /* Use integer comparisons within a GvtNode */
    if (gd1 == gd2)
      result = version - vobject->vlog[x];
    else
      result = gpf_gvt_preorder_compare(gd1, gd2);
    if (result < 0)
      r = x - 1;
    else if (result > 0)
      l = x + 1;
    else
      return x;
  } while (l <= r);

  return r;
}

void
gpf_vobject_set (GpfVObject *vobject, GpfGvt *gvt, gpointer value, int elt_size)
{
  if (!vobject->changed) {
    vobject->len++;
    vobject->log = g_realloc (vobject->log, vobject->len * elt_size);
    vobject->vlog = g_realloc (vobject->vlog, vobject->len * sizeof (GpfVersion));
    vobject->log_index++;
    memmove (vobject->log + (vobject->log_index + 1) * elt_size,
             vobject->log + vobject->log_index * elt_size,
             (vobject->len - vobject->log_index - 1) * elt_size);
    memmove (vobject->vlog + vobject->log_index + 1,
             vobject->vlog + vobject->log_index,
             (vobject->len - vobject->log_index - 1) * sizeof (GpfVersion));
    vobject->vlog [vobject->log_index] = gpf_gvt_last_version (gvt) + 1;
    vobject->changed = TRUE;
  }

  memcpy (vobject->log + vobject->log_index * elt_size, value, elt_size);
}

void
gpf_vobject_commit (GpfVObject *vobject)
{
  vobject->changed = FALSE;
}

gboolean
gpf_vobject_changed (GpfVObject *vobject)
{
  return vobject->changed;
}

gboolean
gpf_vobject_changed_over (GpfVObject *vobject, GpfGvt *gvt,
                          GpfVersion version_from, GpfVersion version_to)
{
  return (gpf_vobject_project(vobject, gvt, version_from)
          != gpf_vobject_project(vobject, gvt, version_to));
}

gboolean
gpf_vobject_set_version (GpfVObject *vobject, GpfGvt *gvt, GpfVersion version)
{
  int old_index = vobject->log_index;
  vobject->log_index = gpf_vobject_project (vobject, gvt, version);

  return (old_index != vobject->log_index);
}


void
gpf_vobject_discard_changes (GpfVObject *vobject)
{
  if (!vobject->changed)
    return;
  vobject->len--;
  vobject->changed = FALSE;
}

/* -- gpf differential versioned object -- */
GpfDiffVObject *
gpf_diff_vobject_new ()
{
}

void
gpf_diff_vobject_free (GpfDiffVObject *vobject)
{
}

gpointer
gpf_diff_vobject_get (GpfDiffVObject *vobject, GpfGvt *gvt, GpfVersion version,
                      void (*apply)(gpointer value, gpointer delta, gboolean forward))
{
}

void
gpf_diff_vobject_set (GpfDiffVObject *vobject, GpfGvt *gvt, gpointer delta, int elt_size)
{
}

void
gpf_diff_vobject_commit (GpfDiffVObject *vobject,
                         void (*apply)(gpointer value, gpointer delta, gboolean forward))
{
}

gboolean
gpf_diff_vobject_changed (GpfDiffVObject *vobject)
{
}

gboolean
gpf_diff_vobject_changed_over (GpfDiffVObject *vobject,GpfGvt *gvt,
                               GpfVersion version_from, GpfVersion  version_to)
{
}

gboolean
gpf_diff_vobject_set_version (GpfDiffVObject *vobject, GpfGvt *gvt, GpfVersion version)
{
}

gboolean
gpf_diff_vobject_has_changes (GpfDiffVObject *vobject, GpfGvt *gvt, GpfVersion version)
{
}

gboolean
gpf_diff_vobject_had_value (GpfDiffVObject *vobject, gpointer value,
                            int (*compare) (const gpointer a, const gpointer b))
{
}

void
gpf_diff_vobject_discard_changes (GpfDiffVObject *vobject)
{
}
