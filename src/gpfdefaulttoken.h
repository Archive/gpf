/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __GPF_DEFAULT_TOKEN_H__
#define __GPF_DEFAULT_TOKEN_H__

#include "gpftoken.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if 0
#define	GPF_TYPE_DEFAULT_TOKEN			(gpf_default_token_get_type ())
#define GPF_DEFAULT_TOKEN(object)		(G_TYPE_CHECK_INSTANCE_CAST ((object), GPF_TYPE_DEFAULT_TOKEN, GpfDefaultToken))
#define GPF_DEFAULT_TOKEN_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GPF_TYPE_DEFAULT_TOKEN, GpfDefaultTokenClass))
#define GPF_IS_DEFAULT_TOKEN(object)		(G_TYPE_CHECK_INSTANCE_TYPE ((object), GPF_TYPE_DEFAULT_TOKEN))
#define GPF_IS_DEFAULT_TOKEN_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GPF_TYPE_DEFAULT_TOKEN))
#define	GPF_DEFAULT_TOKEN_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS ((object), GPF_TYPE_DEFAULT_TOKEN, GpfDefaultTokenClass))
#endif

#define GPF_TYPE_DEFAULT_TOKEN (gpf_default_token_get_type ())
#define GPF_DEFAULT_TOKEN(obj) (GTK_CHECK_CAST (obj, gpf_default_token_get_type (), GpfDefaultToken))
#define GPF_DEFAULT_TOKEN_CLASS(klass) \
  (GTK_CHECK_CLASS_CAST (klass, gpf_default_token_get_type (), GpfDefaultTokenClass))
#define GPF_IS_DEFAULT_TOKEN(obj) (GTK_CHECK_TYPE (obj, gpf_default_token_get_type ()))
#define GPF_IS_DEFAULT_TOKEN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPF_TYPE_DEFAULT_TOKEN))
#define	GPF_DEFAULT_TOKEN_GET_CLASS(object)	GPF_DEFAULT_TOKEN_CLASS(GTK_OBJECT(object)->klass)

/* GpfDefaultToken */
typedef struct _GpfDefaultToken {
  GpfToken parent_object;
  GpfVObject *symbol;
  GpfVObject *lexeme;
  GpfVObject *parent;
  GpfVObject *state;
  GpfVObject *lookback;
  GpfVObject *lookahead;
  GpfVObject *lexer_state;

  unsigned int changed        : 1;
  unsigned int text_changes   : 1;
  unsigned int marked         : 1;
  unsigned int was_relexed    : 1;
} GpfDefaultToken;

typedef struct {
  GpfTokenClass parent_class;
}GpfDefaultTokenClass;

#if 0
GType     gpf_default_token_get_type (void);
#endif
GtkType   gpf_default_token_get_type (void);
GpfToken *gpf_default_token_new      (int          symbol,
                                      const char  *lexeme,
                                      GpfNode     *parent,
                                      int          lookback,
                                      int          lookahead,
                                      int          state,
                                      GpfDocument *document);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GPF_DEFAULT_TOKEN_H__ */
