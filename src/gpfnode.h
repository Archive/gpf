/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __GPF_NODE_H__
#define __GPF_NODE_H__

/* note: I was using glib object until the configuration became very
   difficult.  For now all glib object stuff is conditionally compiled
   out, when glib object becomes default, the glib object related code
   will be put back in. For now gtkobject is used instead. */
#if 0
#include <glib-object.h>
#endif
#include <gtk/gtk.h>
#include "gpfdocument.h"
#include "version.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if 0				
#define	GPF_TYPE_NODE			(gpf_node_get_type ())
#define GPF_NODE(object)		(G_TYPE_CHECK_INSTANCE_CAST ((object), GPF_TYPE_NODE, GpfNode))
#define GPF_NODE_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GPF_TYPE_NODE, GpfNodeClass))
#define GPF_IS_NODE(object)		(G_TYPE_CHECK_INSTANCE_TYPE ((object), GPF_TYPE_NODE))
#define GPF_IS_NODE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GPF_TYPE_NODE))
#define	GPF_NODE_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS ((object), GPF_TYPE_NODE, GpfNodeClass))
#endif

#define GPF_TYPE_NODE (gpf_node_get_type ())
#define GPF_NODE(obj) (GTK_CHECK_CAST (obj, gpf_node_get_type (), GpfNode))
#define GPF_NODE_CLASS(klass) \
  (GTK_CHECK_CLASS_CAST (klass, gpf_node_get_type (), GpfNodeClass))
#define GPF_IS_NODE(obj) (GTK_CHECK_TYPE (obj, gpf_node_get_type ()))
#define GPF_IS_NODE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPF_TYPE_NODE))
#define	GPF_NODE_GET_CLASS(object)	GPF_NODE_CLASS(GTK_OBJECT(object)->klass)

/* change localization */
#define GPF_LOCAL_CHANGES 1
#define GPF_NESTED_CHANGES 1 << 1
#define GPF_CHILD_CHANGES 1 << 2

/* GpfNode is the base class of all document nodes. It is an abstract class,
   so it should not be instantained directly. A derived class must implement
   all the methods below, as this class has no default functionality. */

typedef struct _GpfNode {
#if 0
  GObject parent_object;
#endif
  GtkObject parent_object;
  GpfDocument *document;
} GpfNode;

typedef struct {
#if 0
  GObjectClass parent;
#endif
  GtkObjectClass parent;
  /* get/set methods */
  int      (*get_symbol)         (GpfNode     *node,
                                  GpfVersion   version);
  void     (*set_symbol)         (GpfNode     *node,
                                  int          symbol);
  int      (*arity)              (GpfNode     *node,
                                  GpfVersion   version);
  int      (*get_length)         (GpfNode     *node,
                                  GpfVersion version);
  void     (*set_length)         (GpfNode     *node,
                                  int          length);
  GpfNode *(*get_parent)         (GpfNode     *node,
                                  GpfVersion   version);
  void     (*set_parent)         (GpfNode     *node,
                                  GpfNode     *parent);
  GpfNode *(*get_child)          (GpfNode     *node,
                                  int          i,
                                  GpfVersion   version);
  void     (*set_child)          (GpfNode     *node,
                                  int          i,
                                  GpfNode     *child);

  /* query methods */
  gboolean (*has_children)       (GpfNode     *node,
                                  GpfVersion   version);
  int      (*has_child)          (GpfNode     *node,
                                  GpfNode     *child,
                                  GpfVersion   version);
  gboolean (*has_changes)        (GpfNode     *node,
                                  int          type,
                                  GpfVersion   version);
  /* versioning */
  gboolean (*is_new)             (GpfNode     *node);
  gboolean (*exists)             (GpfNode     *node,
                                  GpfVersion   version);
  void     (*discard_changes)    (GpfNode     *node,
                                  gboolean     and_nested);
  void     (*set_nested_changes) (GpfNode     *node);
  void     (*set_version)        (GpfNode     *node,
                                  GpfVersion   version);
  void     (*mark_deleted)       (GpfNode     *node,
                                  gboolean     and_nested);
  void     (*undelete)           (GpfNode     *vobject,
                                  gboolean     and_nested);
  void     (*commit)             (GpfNode     *node,
                                  gboolean     and_nested);
}GpfNodeClass;

#if 0
GType    gpf_node_get_type           (void);
#endif
GtkType  gpf_node_get_type           (void);
int      gpf_node_get_symbol         (GpfNode     *node,
                                      GpfVersion   version);
void     gpf_node_set_symbol         (GpfNode     *node,
                                      int          symbol);
int      gpf_node_arity              (GpfNode     *node,
                                      GpfVersion version);
int      gpf_node_get_length         (GpfNode     *node,
                                      GpfVersion version);
void     gpf_node_set_length         (GpfNode     *node,
                                      int          length);
GpfNode *gpf_node_get_parent         (GpfNode     *node,
                                      GpfVersion   version);
void     gpf_node_set_parent         (GpfNode     *node,
                                      GpfNode     *parent);
GpfNode *gpf_node_get_child          (GpfNode     *node,
                                      int          i,
                                      GpfVersion   version);
void     gpf_node_set_child          (GpfNode     *node,
                                      int          i,
                                      GpfNode     *child);
GpfNode *gpf_node_left_sibling       (GpfNode     *node,
                                      GpfVersion   version);
GpfNode *gpf_node_right_sibling      (GpfNode     *node,
                                      GpfVersion   version);
gboolean gpf_node_has_children       (GpfNode     *node,
                                      GpfVersion   version);
int      gpf_node_has_child          (GpfNode     *node,
                                      GpfNode     *child,
                                      GpfVersion   version);
gboolean gpf_node_has_changes        (GpfNode     *node,
                                      int          type,
                                      GpfVersion   version);
gboolean gpf_node_is_new             (GpfNode     *node);
gboolean gpf_node_exists             (GpfNode     *node,
                                      GpfVersion   version);
void     gpf_node_discard_changes    (GpfNode     *node,
                                      gboolean     and_nested);
void     gpf_node_set_nested_changes (GpfNode     *node);
void     gpf_node_set_version        (GpfNode     *node,
                                      GpfVersion   version);
void     gpf_node_mark_deleted       (GpfNode     *node,
                                      gboolean     and_nested);
void     gpf_node_undelete           (GpfNode     *node,
                                      gboolean     and_nested);
void     gpf_node_commit             (GpfNode     *node,
                                      gboolean     and_nested);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __NODE_H__ */
