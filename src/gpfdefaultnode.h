/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __GPF_DEFAULT_NODE_H__
#define __GPF_DEFAULT_NODE_H__

#include <gtk/gtk.h>
#include "gpfnode.h"
#include "version.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if 0
#define	GPF_TYPE_DEFAULT_NODE			(gpf_default_node_get_type ())
#define GPF_DEFAULT_NODE(object)		(G_TYPE_CHECK_INSTANCE_CAST ((object), GPF_TYPE_DEFAULT_NODE, GpfDefaultNode))
#define GPF_DEFAULT_NODE_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GPF_TYPE_DEFAULT_NODE, GpfDefaultNodeClass))
#define GPF_IS_DEFAULT_NODE(object)		(G_TYPE_CHECK_INSTANCE_TYPE ((object), GPF_TYPE_DEFAULT_NODE))
#define GPF_IS_DEFAULT_NODE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GPF_TYPE_DEFAULT_NODE))
#define	GPF_DEFAULT_NODE_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS ((object), GPF_TYPE_DEFAULT_NODE, GpfDefaultNodeClass))
#endif

#define GPF_TYPE_DEFAULT_NODE (gpf_default_node_get_type ())
#define GPF_DEFAULT_NODE(obj) (GTK_CHECK_CAST (obj, gpf_default_node_get_type (), GpfDefaultNode))
#define GPF_DEFAULT_NODE_CLASS(klass) \
  (GTK_CHECK_CLASS_CAST (klass, gpf_default_node_get_type (), GpfDefaultNodeClass))
#define GPF_IS_DEFAULT_NODE(obj) (GTK_CHECK_TYPE (obj, gpf_default_node_get_type ()))
#define GPF_IS_DEFAULT_NODE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPF_TYPE_DEFAULT_NODE))
#define	GPF_DEFAULT_NODE_GET_CLASS(object)	GPF_DEFAULT_NODE_CLASS(GTK_OBJECT(object)->klass)

/* GpfDefaultNode */
typedef struct _GpfDefaultNode {
  GpfNode parent_object;
  GpfVObject *symbol;
  GpfVObject *arity;
  GpfVObject *length;
  GpfVObject *parent;
  GpfVObject *children;
  GpfVObject *state;
  GpfVBoolean *nested_changes;
  unsigned int changed        : 1;
  unsigned int child_changes  : 1;
}GpfDefaultNode;

/* GpfDefaultNodeClass */
typedef struct {
  GpfNodeClass parent_class;
}GpfDefaultNodeClass;

#if 0
GType    gpf_default_node_get_type (void);
#endif
GtkType  gpf_default_node_get_type (void);
GpfNode *gpf_default_node_new      (int          symbol,
                                    GpfNode     *parent,
                                    GpfNode    **children,
                                    int          n,
                                    GpfDocument *document);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GPF_DEFAULT_NODE_H__ */
