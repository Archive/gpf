/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "gpfnode.h"

static void gpf_node_base_class_init (GpfNodeClass *class);
static void gpf_node_base_class_finalize (GpfNodeClass *class);
static void gpf_node_class_init (GpfNodeClass *class);
static void gpf_node_init       (GpfNode      *node);

#if 0
GType
gpf_node_get_type ()
{
  static GType type;

  if (!type) {
    static GTypeInfo info = {
      sizeof (GpfNodeClass),
      (GBaseInitFunc) gpf_node_base_class_init,
      (GBaseFinalizeFunc) gpf_node_base_class_finalize,
      (GClassInitFunc) gpf_node_class_init,
      NULL	/* class_destroy */,
      NULL	/* class_data */,
      sizeof (GpfNodeClass),
      0		/* n_preallocs */,
      (GInstanceInitFunc) gpf_node_init ,
    };

    type = g_type_register_static (G_TYPE_INTERFACE, "GpfNode", &info);
  }

  return type;
}
#endif

GtkType
gpf_node_get_type ()
{
  static int type;

  if (!type) {
    GtkTypeInfo info = {
	   "GpfNode",
	   sizeof (GpfNode),
	   sizeof (GpfNodeClass),
	   (GtkClassInitFunc) gpf_node_class_init,
	   (GtkObjectInitFunc) gpf_node_init,
	   (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };

    type = gtk_type_unique (gtk_object_get_type (), &info);
  }

  return type;
}

static void
gpf_node_base_class_init (GpfNodeClass *class)
{
  class->get_symbol = NULL;
  class->set_symbol = NULL;
  class->arity = NULL;
  class->get_length = NULL;
  class->set_length = NULL;
  class->get_parent = NULL;
  class->set_parent = NULL;
  class->get_child = NULL;
  class->set_child = NULL;
  class->has_children = NULL;
  class->has_child = NULL;
  class->has_changes = NULL;
  class->is_new = NULL;
  class->discard_changes = NULL;
  class->mark_deleted = NULL;
  class->undelete = NULL;
  class->commit = NULL;
}

static void
gpf_node_base_class_finalize (GpfNodeClass *class)
{
}

static void
gpf_node_class_init (GpfNodeClass *class)
{
  class->get_symbol = NULL;
  class->set_symbol = NULL;
  class->arity = NULL;
  class->get_length = NULL;
  class->set_length = NULL;
  class->get_parent = NULL;
  class->set_parent = NULL;
  class->get_child = NULL;
  class->set_child = NULL;
  class->has_children = NULL;
  class->has_child = NULL;
  class->has_changes = NULL;
  class->is_new = NULL;
  class->discard_changes = NULL;
  class->mark_deleted = NULL;
  class->undelete = NULL;
  class->commit = NULL;
}

static void
gpf_node_init (GpfNode *node)
{
}

int
gpf_node_get_symbol (GpfNode *node, GpfVersion version)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->get_symbol != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->get_symbol (node, version);
}

void
gpf_node_set_symbol (GpfNode *node, int symbol)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->set_symbol != NULL);

  class->set_symbol (node, symbol);
}

int
gpf_node_arity (GpfNode *node, GpfVersion version)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->arity != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->arity (node, version);
}

int
gpf_node_get_length (GpfNode *node, GpfVersion version)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->get_length != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->get_length (node, version);
}

void
gpf_node_set_length (GpfNode *node, int length)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->set_length != NULL);

  class->set_length (node, length);
}

GpfNode *
gpf_node_get_parent (GpfNode *node, GpfVersion version)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->get_parent != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->get_parent (node, version);
}

void
gpf_node_set_parent (GpfNode *node, GpfNode *parent)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->set_parent != NULL);

  class->set_parent (node, parent);
}

GpfNode *
gpf_node_get_child (GpfNode *node, int i, GpfVersion version)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->get_child != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->get_child (node, i, version);
}

void
gpf_node_set_child (GpfNode *node, int i,
                    GpfNode *child)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->set_child != NULL);

  class->set_child (node, i, child);
}

GpfNode *
gpf_node_left_sibling (GpfNode *node, GpfVersion version)
{
  GpfNode *parent;
  int i;

  parent = gpf_node_get_parent (node, version);

  if (parent == NULL)
    return NULL;

  i = gpf_node_has_child (parent, node, version);

  if (i == -1)
    return NULL;

  return gpf_node_get_child (parent, i-1, version);
}

GpfNode *
gpf_node_right_sibling (GpfNode *node, GpfVersion version)
{
  GpfNode *parent;
  int i;

  parent = gpf_node_get_parent (node, version);

  if (parent == NULL)
    return NULL;

  i = gpf_node_has_child (parent, node, version);

  if (i == -1)
    return NULL;

  return gpf_node_get_child (parent, i+1, version);
}

gboolean
gpf_node_has_children (GpfNode *node, GpfVersion version)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->has_children != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->has_children (node, version);
}

int
gpf_node_has_child (GpfNode *node, GpfNode *child, GpfVersion version)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->has_child != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->has_child (node, child, version);
}

gboolean
gpf_node_has_changes (GpfNode *node, int type, GpfVersion version)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->has_changes != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->has_changes (node, type, version);
}

gboolean
gpf_node_is_new (GpfNode *node)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->is_new != NULL);

  return class->is_new (node);
}

gboolean
gpf_node_exists (GpfNode *node, GpfVersion version)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->exists != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  return class->exists (node, version);
}

void
gpf_node_discard_changes (GpfNode *node, gboolean and_nested)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->discard_changes != NULL);

  class->discard_changes (node, and_nested);
}

void
gpf_node_set_nested_changes (GpfNode *node)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->set_nested_changes != NULL);

  class->set_nested_changes (node);
}

void
gpf_node_set_version (GpfNode *node, GpfVersion version)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->set_version != NULL);

  if (version == GPF_REFERENCE_VERSION)
    version = node->document->reference_version;

  class->set_version (node, version);
}

void
gpf_node_mark_deleted (GpfNode *node, gboolean and_nested)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->mark_deleted != NULL);

  class->mark_deleted (node, and_nested);
}

void
gpf_node_undelete (GpfNode *node, gboolean and_nested)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->undelete != NULL);

  class->undelete (node, and_nested);
}

void
gpf_node_commit (GpfNode *node, gboolean and_nested)
{
  GpfNodeClass *class = GPF_NODE_GET_CLASS (node);
  g_assert (class->commit != NULL);

  class->commit (node, and_nested);
}
