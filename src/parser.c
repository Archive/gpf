/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <glib.h>

#include "gpfdocument.h"
#include "gpfnode.h"
#include "gpfdefaultnode.h"
#include "gpftoken.h"
#include "lexer.h"
#include "common.h"

/* defines from the grammar - remove eventually */
#define	YYFINAL		368
#define	YYFLAG		-32768
#define	YYNTBASE	86
#define	YYLAST		1571

/* external functions defined in the grammar file */
extern int parse_table_action (int state, int la_symbol);
extern int parse_table_goto (int state, int symbol);
extern int parse_table_nsymbols (int rule);
extern int parse_table_lsymbol (int rule);
extern int is_reducible (int state, int la_symbol);
extern int is_shiftable (int state, int symbol); 

static GArray *state_stack;
static GPtrArray *node_stack;
static int parse_state;
static gboolean lexing = FALSE;

static void perform_all_reductions_possible (GpfToken *token, GpfDocument *document);
static void parse_stack_clear ();
static void parse_stack_push (GpfNode *node, int state);
static void shift (GpfNode *node);
static void reduce (int r, GpfDocument *document);
static void right_breakdown ();
static GpfNode *left_breakdown (GpfNode *la);
static GpfNode *pop_lookahead (GpfNode *la);

void
gpf_inc_parse (GpfDocument *document)
{
  GpfNode *la;
  int la_symbol, length;

  gpf_mark_tokens (document);

  /* Initialize the parse stack to contain only bos. */
  parse_stack_clear();
  parse_stack_push(GPF_NODE(document->bos), 0);
  /* Set lookahead to root of tree. */
  if (gpf_token_is_marked (document->bos)
      || gpf_node_get_length (GPF_NODE(document->bos), GPF_CURRENT_VERSION)) {
    la = GPF_NODE(gpf_relex (document->bos, document));
    lexing = TRUE;
  } else
    la = pop_lookahead(GPF_NODE(document->bos));
  la_symbol = gpf_node_get_symbol (la, GPF_CURRENT_VERSION);
  while (TRUE) {
    if (GPF_IS_TOKEN(la)) {
      /* Incremental lexing advances la as a side effect. */
      if (gpf_token_is_marked (GPF_TOKEN(la))) {
        la = GPF_NODE(gpf_relex(GPF_TOKEN(la), document));
        lexing = TRUE;
      } else {
        int n = parse_table_action (parse_state, la_symbol);

        if ((n == 0) || (n == YYFLAG)) {
          /* error */
          /* recover(); */
        } else if (n < 0) {
          /* reduce */
          reduce (-n, document);
        } else if (n == YYFINAL){
          /* accept */
          if (GPF_TOKEN(la) == document->eos) {
            parse_stack_push (GPF_NODE (document->eos), 0);
            gpf_node_set_child (document->ultra_root, 1, g_ptr_array_index (node_stack, 1));
            break; /* Stack is [bos start_symbol eos]. */
          } else {
            /* recover(); */
          }
        } else {
          /* shift */
          parse_stack_push (GPF_NODE(la), n);
          la = pop_lookahead(la);
          la_symbol = gpf_node_get_symbol (la, GPF_CURRENT_VERSION);
        }
      }
    } else {
      /* this is a nonterminal lookahead. */
      if (gpf_node_has_changes(la, GPF_LOCAL_CHANGES | GPF_NESTED_CHANGES,
                               GPF_REFERENCE_VERSION))
        la = left_breakdown(la); /* Split tree at changed points. */
      else {
        int state;

        perform_all_reductions_possible (_gpf_first_token (la, GPF_PREVIOUS_VERSION), document);

        if ((state = is_shiftable (parse_state, gpf_node_get_symbol (la, GPF_CURRENT_VERSION)))) {
          parse_stack_push (la, state);
          right_breakdown ();
          la = pop_lookahead(la);
        }
      }
    }
  } /* while */
  
  if ((length = gpf_node_get_length (GPF_NODE (document->bos), GPF_CURRENT_VERSION))) 
      gpf_token_delete_text (document->bos, 0, length);
  gpf_update_lookbacks (document);
}

static GpfNode *
pop_lookahead (GpfNode *la)
{
  GpfNode *result;

  if (lexing) {
    result = GPF_NODE(gpf_next_new_token ());
    if (result)
      return result;
    else
      lexing = FALSE;
  }
  while (gpf_node_right_sibling (la, GPF_PREVIOUS_VERSION) == NULL)
    la = gpf_node_get_parent (la, GPF_PREVIOUS_VERSION);
  result = gpf_node_right_sibling (la, GPF_PREVIOUS_VERSION);
  /* for ambiguous grammars, put in later
  if (is_fragile (result))
    return left_breakdown (result);
  */
  return result;
}

static void
perform_all_reductions_possible (GpfToken *token, GpfDocument *document)
{
  int la_symbol = gpf_node_get_symbol (GPF_NODE(token), GPF_CURRENT_VERSION);
  int r;

  while ((r = is_reducible (parse_state, la_symbol)))
    reduce (r, document);
}

static void
parse_stack_clear ()
{
  if (!state_stack) {
    state_stack = g_array_new (FALSE, FALSE, sizeof (int));
    node_stack = g_ptr_array_new ();
  } else {
    state_stack = g_array_set_size (state_stack, 0);
    g_ptr_array_set_size (node_stack, 0);
  }
}

static void
parse_stack_push (GpfNode *node, int state)
{
  g_array_set_size (state_stack, state_stack->len + 1);
  g_ptr_array_set_size (node_stack, node_stack->len + 1);
  g_array_index (state_stack, int, state_stack->len - 1) = state;
  g_ptr_array_index (node_stack, node_stack->len - 1) = node;
  parse_state = state;
}


static GpfNode *
parse_stack_pop ()
{
  GpfNode *node = g_ptr_array_index (node_stack, node_stack->len-1);

  g_array_set_size (state_stack, state_stack->len - 1);
  g_ptr_array_set_size (node_stack, node_stack->len - 1);

  return node;
}

static void
right_breakdown ()
{
  GpfNode *node, *child;
  int i;

  do { /* Replace node with its children. */
    node = parse_stack_pop ();
    /* Does nothing when node is a terminal symbol. */
    for (i = 0; i < gpf_node_arity(node, GPF_CURRENT_VERSION); i++) {
      child = gpf_node_get_child (node, 0, GPF_PREVIOUS_VERSION);
      if (GPF_IS_TOKEN(child)) {
        parse_stack_push (child, parse_table_action (parse_state, gpf_node_get_symbol (child, GPF_PREVIOUS_VERSION)));
      } else {
        shift (child);
      }
    }
  } while (!GPF_IS_TOKEN(node));
  /* Leave final terminal symbol on top of stack. */
  parse_stack_push (child, parse_table_action (parse_state, gpf_node_get_symbol (node, GPF_PREVIOUS_VERSION)));
}

static GpfNode *
left_breakdown (GpfNode *la)
{
  if (gpf_node_arity(la, GPF_PREVIOUS_VERSION) > 0) {
    GpfNode *result = gpf_node_get_child(la, 0, GPF_PREVIOUS_VERSION);
    /* for ambiguous grammars, put in later
    if (is_fragile (result))
      return left_breakdown (result);
    */
    return result;
  } else
    return pop_lookahead (la);
}


/* Shift a node onto the parse stack and update the current parse state. */
static void
shift (GpfNode *node)
{
  int symbol = gpf_node_get_symbol (node, GPF_CURRENT_VERSION);
  parse_stack_push (node, parse_table_goto (parse_state, symbol));
}

static void
reduce (int r, GpfDocument *document)
{
  int nsymbols = parse_table_nsymbols (r);
  int lsymbol = parse_table_lsymbol(r);
  GpfNode *parent;
  GpfNode **children = g_new (GpfNode *, nsymbols);

  memcpy (children, node_stack->pdata
                    + node_stack->len - nsymbols,
          nsymbols * sizeof (GpfNode *));
  parent = gpf_default_node_new (lsymbol, NULL, children, nsymbols, document);

  g_ptr_array_set_size (node_stack, node_stack->len - nsymbols + 1);
  g_array_set_size (state_stack, state_stack->len - nsymbols + 1);

  parse_state = parse_table_goto (g_array_index (state_stack, int, state_stack->len - 2), lsymbol);

  g_ptr_array_index (node_stack, node_stack->len - 1) = parent;
  g_array_index (state_stack, int, state_stack->len - 1) = parse_state;
}

/* Figure 6.2: Procedures used to break down the right-hand edge of
   the subtree on top of the parse stack. On each iteration, node
   holds the current top-of-stack symbol. Any subtree with null yield
   appearing in the top-of-stack position is removed in its entirety.  */


#if 0

/* incremental parsing algorithm
   requires modification of parse tables, put in later. */

void inc_parse (GpfDocument *document)
{
  gboolean verifying = FALSE;

  if (!parse_stack)
    g_array_new (FALSE, FALSE, PARSE_STACK_DEPTH_INIT);

  /* Initialize the parse stack to contain only bos. */
  parse_stack_clear();
  parse_state = 0;
  parse_stack_push(document->bos);
  GpfNode *la = pop_lookahead(document->bos); /* Set lookahead to root of tree. */
  while (TRUE) {
    if (GPF_IS_TOKEN(la)) {
      /* Incremental lexing advances la as a side effect. */
      if (gpf_node_has_changes(la, reference_version)) {
        relex(la);
      } else {
        switch (parse_table->action(parse_state, la->symbol)) {
          case ACCEPT:
            if (la == eos) {
              parse_stack->push(eos);
              return; /* Stack is [bos start_symbol eos]. */
            } else {
              recover();
              break;
            }
          case REDUCE r:
            verifying = FALSE;
            reduce(r);
            break;
          case SHIFT s:
            verifying = FALSE;
            shift(s);
            la = pop_lookahead(la);
            break;
          case ERROR:
            if (verifying) {
              right_breakdown();
              verifying = false;
            } else
              recover();
            break;
        }
      }
    } else {
      /* this is a nonterminal lookahead. */
      if (gpf_node_has_changes(la->global_version)
        la = left_breakdown(la); /* Split tree at changed points. */
      else {


        switch (parse_table->action (parse_state, la->symbol)) {
          case REDUCE:
            if (yield(la) > 1)
              verifying = FALSE;
            reduce ();
            break;
          case SHIFT:
            verifying = TRUE;
            shift (la);
            la = pop_lookahead (la);
            break;
          case ERROR:
            if (la->arity > 0)
              la = left_breakdown (la);
            else
              la = pop_lookahead (la);
        }
      }
    }
  } /* while */
}



/* Figure 6.4: An incremental parsing algorithm based on Theorem
   6.3.1.2. The input is a series of subtrees representing portions of
   the previous parse tree intermixed with new material (generated by
   invoking the incremental lexer whenever a modified token is
   encountered). After each nonterminal shift, right_breakdown is
   invoked to force a reconsider-ation of reductions predicated on the
   next terminal symbol. Non-trivial subtrees appearing in the input
   stream are bro-ken down when the symbol they represent is not a
   valid shift in the current state or when they contain modified
   regions.  next_terminal returns the earliest terminal symbol in the
   input stream; when the lookahead's yield is not null, this will be
   the leftmost terminal symbol of its yield. The pop_lookahead and
   left_breakdown methods are shown in Figure 6.5; has_changes is a
   history-based query from Figure 3.2. bos and eos are the token
   sentinels illustrated in Figure 3.2.  */
bool is_fragile (NODE *node) {
  return grammar->is_fragile_production(node->prog) || node->dyn_fragility;
}
class PARSE_STACK_ENTRY {
protected:
  int beginning_state;
  NODE *node;
  void push (int old_state, NODE *node);
  ...
}
/* Extend the normal parse stack entry object with additional fields */
class EXTENDED_STACK_ENTRY : public PARSE_STACK_ENTRY {
private:
  bool left_fragile, right_fragile;
  int total_yield;
public:
  /* Push node onto the stack; its children are the nodes in the stack
     entries represented by the children array. */
  EXTENDED_STACK_ENTRY (node, PARSE_STACK_ENTRY children[]) {
    int i;
    int num_kids = node->arity;
    /* Compute conservative estimate of each child's yield, as well as
       total yield. */
    int yield[num_kids];
    for (i = 0; i < num_kids; i++) {
      if (is_token(children[i]->node)) yield[i] = 1;
      else if (has_type(EXTENDED_STACK_ENTRY, children[i]))
        yield[i] = children[i]->yield;
      else return grammar->estimate_yield(children[i]->node);
      total_yield += yield[i];
    }
    /* Compute and record left side's fragility. */
    left_fragile = false;
    int exposed_yield = 0;
    for (i = 0; i < num_kids; i++) {
      if (grammar->is_fragile_production(children[i]->node->type) ||
        has_type(EXTENDED_STACK_ENTRY, children[i]) &&
        children[i]->left_fragile)
      {left_fragile = true; break;}
      else if ((exposed_yield = yield[i]) >= k) break;
    }
    /*
    Compute and record right side's fragility (symmetric).
    ...
    Set node's dynamic fragility status.
    */
    node->dyn_fragility = left_fragile || right_fragile;
  }
};
/* Figure 6.9: Computation of dynamic fragility. */
/* Reuse a parent when the same production is used and all children
   remain the same.  */
NODE *unambig_reuse_check (int prod, NODE *kids[]) {
  if (arity of prod == 0) return make_new_node(prod);
  NODE *old_parent = kids[0]->parent(previous_version);
  if (old_parent->type != prod) return make_new_node(prod);
  for (int i = 0; i < arity of prod; i++)
    if (node->is_new(kids[i])) return make_new_node(prod);
    else if (old_parent ->= kids[i]->parent(previous_version))
      return make_new_node(prod);
    return old_parent;
}
/* Figure 6.13: Computing unambiguous bottom-up node reuse at
   reduction time. The reuse algorithm will either return a node from
   the previous version of the tree (when the production is unchanged
   and all the children have the same former parent) or create a new
   node to represent the reduction in the new tree
   (make_new_node). Access to the previous children is provided by the
   history interface presented in Figure 3.2.  Reuse a parent when the
   same production is used and at least one child is unchanged.  */
NODE *ambig_reuse_check (int prod, NODE *kids[]) {
  if (arity of prod == 0) return make_new_node(prod);
  for (int i = 0; i < arity of prod; i++)
    if (!node->is_new(kids[i])) {
      NODE *old_parent = kids[i]->parent(previous_version);
      if (old_parent->type == prod && !in_reuse_list(old_parent)) {
        add_to_reuse_list(old_parent);
        return old_parent;
      }
    }
    return make_new_node(prod);
}
/* Figure 6.14: Computing ambiguous bottom-up node reuse at reduction
   time. This method differs from that of Figure 6.13 by allowing a
   partial match to succeed: if a reuse candidate can be found among
   the former parents of reused children, it will be used to represent
   the production being reduced. A simple FCFS policy resolves
   competition for the same parent when its former children appear in
   multiple sites in the new tree. Duplicate reuse is avoided by
   maintaining a list of the explicitly reused nodes.  */

/* Compute top-down reuse in a single traversal of the new tree. */

top_down_reuse () {
  process_deletions(UltraRoot); Section 3.2.
    top_down_reuse_traversal(root);
}
/* Apply a localized top-down reuse check at each modification site. */
top_down_reuse_traversal (NODE *node) {
  if (node->has_changes(local) && !node->is_new())
    reuse_isomorphic_structure(node);
  else if (node->has_changes(nested))
    foreach child of node do top_down_reuse_traversal(child);
}
/* Restore reuse paths descending from node. */
reuse_isomorphic_structure (NODE *node) {
  for (int i = 0; i < node->arity; i++) {
    NODE *current_child = node->child(i);
    NODE *previous_child = node->child(i, previous_version);
    if (current_child->is_new() && !previous_child->exists() &&
      current_child->type == previous_child->type) {
      replace_with(current_child, previous_child);
      reuse_isomorphic_structure(previous_child);
    } else if (current_child->has_changes(nested))
      top_down_reuse_traversal(current_child);
  }
}
/* Figure 6.15: Computing top-down reuse. The algorithm performs a
   top-down traversal of the structure that includes each modification
   site, attempting to replace newly created nodes with discarded
   nodes. process_deletions identifies the set of nodes from the
   previous version of the tree that were discarded in producing the
   current version; these nodes are the (only) candidates for top-down
   reuse.  */

#endif
