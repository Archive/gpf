/* gpf - GNU Program Analysis Framework
   Copyright (C) 2000  Mark Slicker <jamess1@wwnet.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __GPF_TOKEN_H__
#define __GPF_TOKEN_H__

#include <gtk/gtk.h>
#include "version.h"
#include "gpfnode.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if 0
#define	GPF_TYPE_TOKEN			(gpf_token_get_type ())
#define GPF_TOKEN(object)		(G_TYPE_CHECK_INSTANCE_CAST ((object), GPF_TYPE_TOKEN, GpfToken))
#define GPF_TOKEN_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GPF_TYPE_TOKEN, GpfTokenClass))
#define GPF_IS_TOKEN(object)		(G_TYPE_CHECK_INSTANCE_TYPE ((object), GPF_TYPE_TOKEN))
#define GPF_IS_TOKEN_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GPF_TYPE_TOKEN))
#define	GPF_TOKEN_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS ((object), GPF_TYPE_TOKEN, GpfTokenClass))
#endif

#define GPF_TYPE_TOKEN (gpf_token_get_type ())
#define GPF_TOKEN(obj) (GTK_CHECK_CAST (obj, gpf_token_get_type (), GpfToken))
#define GPF_TOKEN_CLASS(klass) \
  (GTK_CHECK_CLASS_CAST (klass, gpf_token_get_type (), GpfTokenClass))
#define GPF_IS_TOKEN(obj) (GTK_CHECK_TYPE (obj, gpf_token_get_type ()))
#define GPF_IS_TOKEN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPF_TYPE_TOKEN))
#define	GPF_TOKEN_GET_CLASS(object)	GPF_TOKEN_CLASS(GTK_OBJECT(object)->klass)

/* change localization */
#define GPF_TEXT_CHANGES 1 << 3

/* GpfToken */
typedef struct _GpfToken {
  GpfNode parent;
  gpointer data;
} GpfToken;

typedef struct {
  GpfNodeClass parent;
  char     *(*lexeme)        (GpfToken    *token,
                              GpfVersion   version);
  int       (*get_state)     (GpfToken    *token,
                              GpfVersion   version);
  void      (*set_state)     (GpfToken    *token,
                              int          state);
  int       (*get_lookback)  (GpfToken    *token,
                              GpfVersion   version);
  void      (*set_lookback)  (GpfToken    *token,
                              int          lookback);
  int       (*get_lookahead) (GpfToken    *token,
                              GpfVersion   version);
  void      (*set_lookahead) (GpfToken    *token,
                              int          lookahead);
  void      (*mark)          (GpfToken    *token);
  gboolean  (*is_marked)     (GpfToken    *token);
  void      (*set_re_lexed)  (GpfToken    *token);
  gboolean  (*was_re_lexed)  (GpfToken    *token);
  void      (*insert_text)   (GpfToken    *token,
                              int          offset,
                              const char  *text,
                              int          length);
  void      (*delete_text)   (GpfToken    *token,
                              int          start,
                              int          end);
}GpfTokenClass;

#if 0
GType     gpf_token_get_type      (void);
#endif
GtkType   gpf_token_get_type      (void);
char     *gpf_token_lexeme        (GpfToken    *token,
                                   GpfVersion   version);
int       gpf_token_get_state     (GpfToken    *token,
                                   GpfVersion   version);
void      gpf_token_set_state     (GpfToken    *token,
                                   int          state);
int       gpf_token_get_lookback  (GpfToken    *token,
                                   GpfVersion   version);
void      gpf_token_set_lookback  (GpfToken    *token,
                                   int          lookback);
int       gpf_token_get_lookahead (GpfToken    *token,
                                   GpfVersion   version);
void      gpf_token_set_lookahead (GpfToken    *token,
                                   int          lookahead);
void      gpf_token_mark          (GpfToken    *token);
gboolean  gpf_token_is_marked     (GpfToken    *token);
void      gpf_token_set_re_lexed  (GpfToken    *token);
gboolean  gpf_token_was_re_lexed  (GpfToken    *token);
GpfToken *gpf_token_previous      (GpfToken    *token,
                                   GpfVersion   version);
GpfToken *gpf_token_next          (GpfToken    *token,
                                   GpfVersion   version);
void      gpf_token_insert_text   (GpfToken    *token,
                                   int          offset,
                                   const char  *text,
                                   int          length);
void      gpf_token_delete_text   (GpfToken    *token,
                                   int          start,
                                   int          end);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GPF_TOKEN_H__ */
